(function () {
    var URL        = "https://3dev.com.co/doom/bootstrap/Servicios/AdmContactos.asmx/CrearContacto";
    var URL_SEGUIR = "https://3dev.com.co/doom/bootstrap/Servicios/AdmContactos.asmx/CrearSeguir";

    //var URL_SEGUIR = "http://localhost:47646/GestionMovil/Servicios/AdmContactos.asmx/CrearContacto";
     //var URL        = "http://localhost:47646/GestionMovil/Servicios/AdmContactos.asmx/CrearContacto";

    RecibeSeguir = function() {
       var sms_error = "";
       var correo_seguir = "";       
       correo_seguir  = $("#txtseguir").val();
      if (isValidEmailAddress(correo_seguir) ==false){
          sms_error = sms_error +  "direccion de correo no valida.";
          $("#txtseguir").val("")
       }
       if (sms_error =="" ){
          EviarSeguir($("#txtseguir").val());
       }
       else{
          sweetAlert(sms_error)
       }       
    }

    ReciveEnvioContacto = function() {
       var sms_error = "";

       if ($("#register-form-name").val() == "") {
          sms_error = sms_error +  "Ingresa tu nombre para poder hacer referencia en correos futuros."  + "</br>";
       }
       if ($("#register-form-email").val() == "") {
          sms_error = sms_error +  "Ingresa tu correo para poder hacer referencia en correos futuros." + "</br>";
       }  
       if (isValidEmailAddress($("#register-form-email").val()) == false) {
          sms_error = sms_error +  "Direccion de correo no valida." + "</br>";
       }       
       if ($("#register-form-phone").val() == "") {
          sms_error = sms_error +  "Ingresa numero de telefono no valido." + "</br>";
       }
       if ($("#register-form-requiere").val() == "") {
          sms_error = sms_error + "Ingresa la informacion que gustaria recibir por nuestra compañia." + "</br>";
       }     
         
       
       if (sms_error =="" ){
           EviarContacto(
                 $("#register-form-name").val()
               , $("#register-form-email").val()
               , $("#register-form-phone").val()            
               , $("#register-form-requiere").val()
			
           
               );
       }
       else{
         swal({title: "<small>Error, faltan algunos datos </small>!",text: "<span style='color:#F8BB86;' >"+ sms_error +"<span>",   html: true });
       }
    }
    
   function isValidEmailAddress(emailAddress) {
       var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
       return pattern.test(emailAddress);
   };    
   
   EviarContacto = function (nombre, correo, tels , requiere,ciudad,sucursal ) {
        var consulta = ""
        consulta = consulta + "<PagXml>                              ";
        
  
        consulta = consulta + "   <Param>                            ";
        consulta = consulta + "      <CAMPO>NOMBRE</CAMPO>            ";
        consulta = consulta + "      <VALOR><![CDATA[" + nombre  + "]]></VALOR>     ";
        consulta = consulta + "   </Param>                           ";
        consulta = consulta + "   <Param>                            ";
        consulta = consulta + "      <CAMPO>CORREO</CAMPO>            ";
        consulta = consulta + "      <VALOR><![CDATA[" + correo  + "]]></VALOR>     ";
        consulta = consulta + "   </Param>                           ";
        consulta = consulta + "   <Param>                            ";
        consulta = consulta + "      <CAMPO>TELS</CAMPO>            ";
        consulta = consulta + "      <VALOR><![CDATA[" + tels + "]]></VALOR>     ";
        consulta = consulta + "   </Param>                           ";
		
	
        
        consulta = consulta + "   <Param>                            ";
        consulta = consulta + "      <CAMPO>PROCESO</CAMPO>            ";
        consulta = consulta + "      <VALOR><![CDATA[0084]]></VALOR>     ";
        consulta = consulta + "   </Param>                           ";     

        consulta = consulta + "   <Param>                             ";
        consulta = consulta + "      <CAMPO>RESPONDER</CAMPO>            ";
        consulta = consulta + "      <VALOR><![CDATA[0112]]></VALOR>     ";
        consulta = consulta + "   </Param>                            ";        
        
        consulta = consulta + "   <Param>                             ";
        consulta = consulta + "      <CAMPO>MENSAJE</CAMPO>            ";
        consulta = consulta + "      <VALOR><![CDATA[" + requiere + "]]></VALOR>     ";
        consulta = consulta + "   </Param>                            ";
        
        consulta = consulta + "   <Param>                             ";
        consulta = consulta + "      <CAMPO>PROYECTO</CAMPO>            ";
        consulta = consulta + "      <VALOR><![CDATA[" + "XTAMPARTEX" + "]]></VALOR>     ";
        consulta = consulta + "   </Param>                            ";
		consulta = consulta + "   <Param>                             ";
        consulta = consulta + "      <CAMPO>EMPRESA</CAMPO>            ";
        consulta = consulta + "      <VALOR><![CDATA[" + "00006" + "]]></VALOR>     ";
        consulta = consulta + "   </Param>                            ";
        consulta = consulta + "   <Param>                            ";
        consulta = consulta + "      <CAMPO>DESTINOS</CAMPO>            ";
        consulta = consulta + "      <VALOR><![CDATA[" + "jeison.gutierrez@3dev.com.co,equipelco@equipelco.com" + "]]></VALOR>     ";
        consulta = consulta + "   </Param>                           "; 
        consulta = consulta + "</PagXml>                             ";
     /*     alert(consulta);
        $("#CIUDAD").val(consulta); */

        jQuery.ajax({
            type: "POST",
            url: URL,
            data: "Parametros=" + consulta + "",
            success: function (data) {
                sweetAlert("Tu contacto fue registrado...");
                // En caso de que retorne datos
                var xml_string = jQuery(data).text();
                //alert(xml_string);
                $(xml_string).find('Datos').each(function () {
                });
            },
            error: function (xhr, msg)
            {
                alert("err:" + msg + '\n' + xhr.responseText);
                //sweetAlert("Contacto enviado...");
            }
        })
    }
  
   EviarSeguir = function(correo) {
        var consulta = ""
        consulta = consulta + "<PagXml>                              ";
        consulta = consulta + "   <Param>                            ";
        consulta = consulta + "      <CAMPO>CORREO</CAMPO>            ";
        consulta = consulta + "      <VALOR><![CDATA[" + correo  + "]]></VALOR>     ";
        consulta = consulta + "   </Param>                           ";
        
        consulta = consulta + "   <Param>                            ";
        consulta = consulta + "      <CAMPO>PROCESO</CAMPO>            ";
        consulta = consulta + "      <VALOR><![CDATA[0084]]></VALOR>     ";
        consulta = consulta + "   </Param>                           ";
        
        consulta = consulta + "   <Param>                            ";
        consulta = consulta + "      <CAMPO>RESPONDER</CAMPO>            ";
        consulta = consulta + "      <VALOR><![CDATA[0092]]></VALOR>     ";
        consulta = consulta + "   </Param>  ";
        
        consulta = consulta + "   <Param>                            ";
        consulta = consulta + "      <CAMPO>DESTINOS</CAMPO>            ";
        consulta = consulta + "      <VALOR><![CDATA[" + "jeison.gutierrez@3dev.com.co,equipelco@equipelco.com" + "]]></VALOR>     ";
        consulta = consulta + "   </Param>                           "; 
        consulta = consulta + "</PagXml>                             ";

        alert(consulta);
        //alert("filtro: "+ consulta);
        //$("#tdbusCLientes").empty();
        //$('#tdbusCLientes tbody').remove();
        //$('#tdbusCLientes tr:last-child').remove();

        jQuery.ajax({
            type: "POST",
            url: URL_SEGUIR,
            data: "Parametros=" + consulta + "",
            success: function (data) {
                // En caso de que retorne datos
                var xml_string = jQuery(data).text();
                //alert(xml_string);
                $(xml_string).find('Datos').each(function () {
                });
                sweetAlert("Tu contacto fue registrado...");
            },
            error: function (xhr, msg)
            {
                alert(msg + '\n' + xhr.responseText);
            }
        })
    }
    
})();