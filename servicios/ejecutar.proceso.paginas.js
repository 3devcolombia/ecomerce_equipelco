(function ($) {

    /*MOSTRAR LOS POST*/
    PageShowSingle = function (pagina) {
        var URL = "https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_blog.asmx/DatosblogII";
        //var URL = "http://localhost:47646/GestionMovil/Servicios/sweb_blog.asmx/DatosblogII";
        var man_html = "";
        $('#posts').empty();
        jQuery.ajax({
            type: "POST",
            url: URL,
            data: "empresa=" + "00009" + "&categoria=''&cantidad=1&pagina=1&formato=0117&npagina="+ pagina +"",
            success: function (data) {
                var xml_string = jQuery(data).text();
                $(xml_string).find('Datos').each(function () {
                    doc = $(this).find("TEXTO").text()
                    tit = $(this).find("TITULO").text()
                    fec = $(this).find("FECHA").text()
                    usu = $(this).find("NOM_USUARIO").text()                    
                    cat = $(this).find("NOMCATEGORIA").text()                    
                    img1 = $(this).find("REG_ARCHIVO1").text()
                    img2 = $(this).find("REG_ARCHIVO2").text()
                    img3 = $(this).find("REG_ARCHIVO3").text()
                    reg = $(this).find("reg").text()
                    
                    man_html = man_html + "						<!-- LEFT -->                                                                    "
                    man_html = man_html + "						<div class=col-md-9 col-sm-9>                                                    "
                    man_html = man_html + "                                                                                                 "
                    man_html = man_html + "							<h1 class=blog-post-title>"+ tit +"</h1>                           "
                                                              
                    man_html = man_html + "							<!-- article content -->                                                      "
                    man_html = man_html + "							"+ doc +"                                                    "
                    man_html = man_html + "							<div class=divider><!-- divider --></div>                                      "
                    man_html = man_html + "						</div>                                                                            "
                                        
                    
                });
                $('#posts').append(man_html);

            },
            error: function (xhr, msg) {
                alert(msg + '\n' + xhr.responseText);
            }
        })
    }



    getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
    
            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };


})(jQuery);