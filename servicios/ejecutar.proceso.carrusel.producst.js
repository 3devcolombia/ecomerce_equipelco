/*http://localhost:47646/GestionMovil/servicios/sweb_blog_imagen.ashx?reg=5*/

(function ($) {
    /*CANTIDAD DE ELEMENTOS QUE VA MANEJAR EL PAGINADOR*/
    var elementos_paginador = 4;  /*EDITABLE {en la parte de abajo el paginados}*/
    /*ELEMENTOS POST POR PAGINA*/
    var elementos_xpage = 4;     /*EDITABLE {cusntos pos salen por paginacion}*/
    /*ELEMENTOS EN RETROCESO*/
    var elementos_xprevio = 4;     /*EDITABLE {cuanto retrocede cuantos para casos de uno es uno}*/

    /*PAGINA QUE ESTA MOSTRANDO EL BLOG*/
    var pagina_post = 1;
    /*PAGINA PAGINADOR*/
    var pagina_paginador = 1;
    /*NUMERO DE PAGINA DEL PAGINDOS DONDE ESTA UBICADO*/
    var pagina_ubicado = 1;
    var paginas_presentadas = 0;


    /*MOSTRAR LOS POST*/
    CargarPost = function () {
        var Filtro = "";
        var URL = "https://3dev.com.co/Doom/Bootstrap/datos/webdatos.asmx/FiltroBusqueda";
        //var URL = "http://localhost:47646/GestionMovil/Servicios/sweb_blog.asmx/DatosblogII";
        var man_html = "";

        if (Filtro == "") {
            Filtro = "SUCURSAL= 00004 ";
        } else {
            Filtro = Filtro + " , SUCURSAL= 00004";
        }

        var consulta = ""
        consulta = consulta + "<PagXml>                                ";
        consulta = consulta + "   <CorXml>                             ";
        consulta = consulta + "      <TIPO>PARAMETRO</TIPO>            ";
        consulta = consulta + "      <CODIGO>0163</CODIGO>             ";
        consulta = consulta + "      <PAGINA>1</PAGINA>             ";
        consulta = consulta + "      <PAGINAS>20</PAGINAS>           ";
        consulta = consulta + "      <NOMBRE>PROCESO</NOMBRE>          ";
        consulta = consulta + "      <ORDEN>1</ORDEN>                  ";
        consulta = consulta + "      <VALOR><![CDATA[EMPRESA.CONTACTOS]]></VALOR>   ";
        consulta = consulta + "      <FILTRO><![CDATA[" + Filtro + "]]></FILTRO>     ";
        consulta = consulta + "   </CorXml>                                          ";
        consulta = consulta + "</PagXml>                                             ";
        // console.log(consulta);

        $('#carrusel').empty();
        jQuery.ajax({
            type: "POST",
            url: URL,
			async: false,
            data: "Parametros=" + consulta + "",
            success: function (data) {
                var xml_string = jQuery(data).text();
                $(xml_string).find('Datos').each(function () {
                    
                    nombre = $(this).find("NOMBRE").text();
                    ref = $(this).find("REFERENCIA").text();
                    marca = $(this).find("MARCA").text();
                    precio = $(this).find("PRECIO").text();
                    design = $(this).find("DISEÑO").text();
                    genero = $(this).find("GENERO").text();
                    grupo = $(this).find("GRUPO").text();
                    saldo = $(this).find("SALDO").text();             
            
                    var man_html = `
                    <div class="owl-demo"> 
                        <div class="item">
                            <div class="product style1 col-md-12 col-xs-12">
                                <figure class="product-preview" style="height:300px !important">
                                    <img src="https://3dev.com.co/Doom/Bootstrap/ShowImage.ashx?ID=${ref}&TIPO=FRENTE&COLOR=*&EMPRESA=00004" >
                                    <div class="overlay">
                                        <a href="#product-quickview1" 
                                        onclick=cargarelpopup("${encodeURIComponent(nombre)}","${encodeURIComponent(ref)}","${encodeURIComponent(marca)}","${precio}","${encodeURIComponent(design)}","${encodeURIComponent(genero)}","${encodeURIComponent(grupo)}","${encodeURIComponent(saldo)}") 
                                        class="product-quickview button button-xxlarge white-bg open-popup-link"
                                        style="left: 50%;">Ver Mas</a>
                                    </div>
                                </figure>
                                    <div class="product-details row">
                                        <div class="product-info col-md-12">
                                            <h6 class="heading-alt-style9" style="font-size:14px !important"><a href="">${nombre}</a></h6>
                                            <div class="product-rating" style="font-weight: 700;" >
                                                ${marca}
                                            </div>
                                        <p style="font-weight: 700; color:#ff5049;"><span class="currency">$</span>${precio}</p> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `
                    $('#carrusel').append(man_html);                    
                });                     
            },
            error: function (xhr, msg) {
            }
        })
    }


})(jQuery);