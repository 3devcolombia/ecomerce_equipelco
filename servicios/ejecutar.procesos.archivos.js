/*http://localhost:47646/GestionMovil/servicios/sweb_blog_imagen.ashx?reg=5*/

(function ($) {
    /*CANTIDAD DE ELEMENTOS QUE VA MANEJAR EL PAGINADOR*/
    var elementos_paginador = 100;  /*EDITABLE {en la parte de abajo el paginados}*/
    /*ELEMENTOS POST POR PAGINA*/
    var elementos_xpage = 100;     /*EDITABLE {cusntos pos salen por paginacion}*/
    /*ELEMENTOS EN RETROCESO*/
    var elementos_xprevio = 100;     /*EDITABLE {cuanto retrocede cuantos para casos de uno es uno}*/


    /*PAGINA QUE ESTA MOSTRANDO EL BLOG*/
    var pagina_post = 1;
    /*PAGINA PAGINADOR*/
    var pagina_paginador = 1;
    /*NUMERO DE PAGINA DEL PAGINDOS DONDE ESTA UBICADO*/
    var pagina_ubicado = 1;
    var paginas_presentadas = 0;
    var manejo_pagina = "";


    preparafiltro = function () {
        var filtro =$("#txtsearch").val();

        var pagina = getUrlParameter('pagina');
        CargarArchivos(pagina)
        CargarPaginas(pagina)
    }


    /*MOSTRAR LOS POST*/
    CargarArchivos = function (pagina) {
        manejo_pagina =pagina;
        var filtro =$("#txtsearch").val();
        var URL = "https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_blog.asmx/DatosblogII";
        //var URL = "http://localhost:47646/GestionMovil/Servicios/sweb_blog.asmx/DatosblogII";
        var man_html = "";
        $('#posts').empty();
        jQuery.ajax({
            type: "POST",
            url: URL,
            data: "empresa=" + "00009" + "&categoria="+ filtro +"&cantidad="+ elementos_xpage +"&pagina=" + pagina_post + "&formato=0120&npagina="+ pagina +"",
            success: function (data) {
                var xml_string = jQuery(data).text();
                //alert(xml_string);
                $(xml_string).find('Datos').each(function () {
                    doc = $(this).find("NARCHIVO").text()
                    nom = $(this).find("NOMBRE").text()
                    tit = $(this).find("DESCRIPCION").text()
                    fec = $(this).find("FECHA").text()
                    usu = $(this).find("NOM_USUARIO").text()                    
                    cat = $(this).find("DESCRIPCION").text()                    
                    reg = $(this).find("reg").text()
                    man_html = man_html + "				<div class='clearfix search-result'>"
                    man_html = man_html + "					<!-- item --> "
                    man_html = man_html + "					<h4 class='margin-bottom-0'> "
                    man_html = man_html + "						<a href='#'>"+ doc +"</a> "
                    man_html = man_html + "					</h4> "
         
                    man_html = man_html + "					<p>"+cat+" "
                    man_html = man_html + "  				<div class='row' style='height: 20px;'></div> "
                    man_html = man_html + "  				<div class='row'> "
					man_html = man_html + "					   <a class='col-md-12' target='_blank' href='https://3dev.com.co/Doom/Bootstrap/Formularios/Modulos/mercadeo/blog_descargaarc.aspx?reg=" + reg +"';'> "
					man_html = man_html + "						<button  class='btn btn-primary' > "
					man_html = man_html + "						<i class='fa fa-check'></i> DECARGAR ARCHIVO</button> "
					man_html = man_html + "						</a> "
					man_html = man_html + "					 </div> "
                    man_html= man_html + "				</div>"
                });
                $('#posts').append(man_html);
            },
            error: function (xhr, msg) {
                alert(msg + '\n' + xhr.responseText);
            }
        })
    }


    CargarPaginas = function (pagina) {
        var filtro =$("#txtsearch").val();

        var URL = "https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_blog.asmx/PaginasArchivos";
        //var URL = "http://localhost:47646/GestionMovil/Servicios/sweb_blog.asmx/PaginasArchivos";
        var man_elementos = 1;
        jQuery.ajax({
            type: "POST",
            url: URL,
            data: "empresa=" + "00009" + "&cantidad='"+elementos_xpage +"'&npagina="+pagina +"&categoria="+ filtro +"",
            success: function (data) {
                var xml_string = jQuery(data).text();
                
                $(xml_string).find('Datos').each(function () {
                    cant = $(this).find("CANT").text()
                    paginas = $(this).find("PAGINAS").text()
                    var man_i;
                    var m_cant = parseInt(paginas);
                    var man_html = "";
                    $('#pagination').empty();
                    var carga_paginador = 0;


                    //PRESENTACION DE m_cant por el grupo de paginas donde se encuentra
                    if (pagina_paginador > parseInt(paginas)) {
                        pagina_paginador = pagina_paginador - 1;
                    };

                    m_cant = m_cant * pagina_paginador;

                    if (m_cant > parseInt(paginas) ){ m_cant = parseInt(paginas)  };


                    /*PAGINA PREVIA EN EL PAGINADOR*/
                    if (paginas > 1){
                        man_html = man_html + "<li class='page-item'><a class='page-link' onclick='previuspaginator(" + carga_paginador + ")' >Previo</a></li>";   
                        //man_html = man_html + " <li class='disabled'><a href='#'>Previous</a></li> "
                    }
                    console.log("paso 1 {man_i ("+ man_i +"), pagina_post ("+ pagina_post +"), pagina_paginador (" + pagina_paginador +") , m_cant ("+ m_cant +") , pagina_ubicado ("+ pagina_ubicado +") , paginas ("+ paginas +") }" );


                    if (pagina_paginador > pagina_post) {
                        pagina_post = pagina_paginador
                    }

                    //ALERT(M_CANT);
                    for (man_i = pagina_paginador; man_i <= m_cant; man_i++) {
                        console.log("paso 2 {man_elementos (" + man_elementos +") , elementos_paginador ("+ elementos_paginador +") , pagina_ubicado ("+ pagina_ubicado +") , pagina_post ("+  pagina_post +") }" );
                        if (man_elementos <= elementos_paginador) {
                            if (man_i == pagina_post) {
                                man_html = man_html + "<li class='page-item active'><a class='page-link' onclick='gopage(" + man_i + ")'>" + man_i + "</a></li>";
                            } else {
                                man_html = man_html + "<li class='page-item'><a class='page-link' onclick='gopage(" + man_i + ")' >" + man_i + "</a></li>";
                            }
                            carga_paginador = carga_paginador + 1;
                            console.log("paso 3 {man_i ("+  man_i +") , carga_paginador ("+  carga_paginador +") , pagina_ubicado ("+ pagina_ubicado +")}" );
                            man_elementos = man_elementos + 1;
                            paginas_presentadas = man_i;
                        }
                    }


                    console.log("paso 4 {m_cant ("+  m_cant +") , elementos_paginador ("+  elementos_paginador +") , paginas ("+ paginas +") , cant ("+ cant +")}" );
                    if ((cant) > elementos_paginador)
                    {
                        man_html = man_html + "<li class='page-item'><a class='page-link' onclick='nextpaginator(" + carga_paginador + ")' >SIGUIENTE</a></li>";   
                    }
                    //man_html = man_html + "<li class='page-item'><a class='page-link' onclick='nextpage(" + carga_paginador + ")' >SIGUIENTE</a></li>";
                    man_html = man_html + "</ul>"
                    $('#pagination').append(man_html);
                });
            },
            error: function (xhr, msg) {
                alert(msg + '\n' + xhr.responseText);
            }
        })
    }

    /*LLEVA A UNA PAGINA DETERMINADA*/
    gopage = function (page) {
        pagina_post = page;
        console.log("gopage" );
        CargarArchivos(manejo_pagina);
        CargarPaginas(manejo_pagina)
        $('html, body').animate({ scrollTop: 0 }, 1250);
    }
    /*VA ALA PAGINA SIGUIENTE*/
    nextpage = function (page) {
        pagina_post = pagina_post + 1;
        CargarArchivos();
        CargarPaginas()
        console.log("nextpage" );
        $('html, body').animate({ scrollTop: 0 }, 1250);        
    }
    /*VA A LA PAGINA PREVIA*/
    previuspage = function (page) {
        pagina_post = pagina_post - 1;
        if (pagina_post <=0 ){
            pagina_post = 1;
        }
        console.log("paginas previas  {pagina_post ("+ pagina_post +")} ");
        CargarArchivos(manejo_pagina);
        CargarPaginas(manejo_pagina)
        $('html, body').animate({ scrollTop: 0 }, 1250);
    }

    /*PAGINADOR*/
    nextpaginator = function (page) {
        pagina_post = pagina_post + 1;
        pagina_paginador = pagina_paginador + page;
        pagina_ubicado = pagina_ubicado + 1;
        console.log("nextpaginator  {pagina_paginador ("+ pagina_paginador +")} ");
        CargarPaginas(manejo_pagina);
        //gopage(pagina_post);
        $('html, body').animate({ scrollTop: 0 }, 1250);
    }
    previuspaginator = function (page) {
        pagina_post = pagina_post - 1;
        pagina_paginador = pagina_paginador - elementos_xprevio;
        pagina_ubicado = pagina_ubicado - 1;
        if (pagina_post <=0 ){pagina_post = 1;}
        if (pagina_paginador<=0){pagina_paginador = 1;}
        console.log("previuspaginator  {pagina_post ("+ pagina_post +")}" );
        CargarPaginas(manejo_pagina)
        gopage(pagina_post);
        $('html, body').animate({ scrollTop: 0 }, 1250);
    }
    
    getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
    
            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };


})(jQuery);