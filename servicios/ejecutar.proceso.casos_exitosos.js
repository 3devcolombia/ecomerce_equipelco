/*http://localhost:47646/GestionMovil/servicios/sweb_blog_imagen.ashx?reg=5*/

(function ($) {
    /*CANTIDAD DE ELEMENTOS QUE VA MANEJAR EL PAGINADOR*/
    var elementos_paginador = 4;  /*EDITABLE {en la parte de abajo el paginados}*/
    /*ELEMENTOS POST POR PAGINA*/
    var elementos_xpage = 4;     /*EDITABLE {cusntos pos salen por paginacion}*/
    /*ELEMENTOS EN RETROCESO*/
    var elementos_xprevio = 4;     /*EDITABLE {cuanto retrocede cuantos para casos de uno es uno}*/


    /*PAGINA QUE ESTA MOSTRANDO EL BLOG*/
    var pagina_post = 1;
    /*PAGINA PAGINADOR*/
    var pagina_paginador = 1;
    /*NUMERO DE PAGINA DEL PAGINDOS DONDE ESTA UBICADO*/
    var pagina_ubicado = 1;
    var paginas_presentadas = 0;

    /*MOSTRAR LOS POST*/
    CargarPost = function (pagina) {
        var URL = "https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_blog.asmx/DatosblogII";
        //var URL = "http://localhost:47646/GestionMovil/Servicios/sweb_blog.asmx/DatosblogII";
        var man_html = "";
        $('#posts').empty();
        jQuery.ajax({
            type: "POST",
            url: URL,
			async: false,
            data: "empresa=" + "00004" + "&categoria=''&cantidad="+ elementos_xpage +"&pagina=" + pagina_post + "&formato=0116&npagina="+ pagina +"",
            success: function (data) {
                var xml_string = jQuery(data).text();
                $(xml_string).find('Datos').each(function () {
                    doc = $(this).find("TEXTO").text()
                    tit = $(this).find("TITULO").text()
                    fec = $(this).find("FECHA").text()
                    usu = $(this).find("NOM_USUARIO").text()                    
                    cat = $(this).find("NOMCATEGORIA").text()                    
                    img1 = $(this).find("REG_ARCHIVO").text()
                    img2 = $(this).find("REG_ARCHIVO1").text()
                    img3 = $(this).find("REG_ARCHIVO2").text()
                    reg = $(this).find("reg").text()     

                     man_html = man_html + "    <section class='main-contents padding-top80 padding-Bottom50'><!-- member --> "
                     man_html = man_html + "    <div class='portfolio-project-details single-column-image info-right wow fadeInLeft'> "
                     man_html = man_html + "         <div class='container'>"
                     man_html = man_html + "             <div class='row'>"
                     man_html = man_html + "                  <div class='project-info col-md-6 ' style='height: 400px;overflow-y: scroll;'>"
                     man_html = man_html + "                       <div class='project-name'>"
                     man_html = man_html + "				               <h6 class='heading-alt-style9 member-name'>"+ tit +"</h6> "
                     man_html = man_html + "                        </div>"
                     man_html = man_html + "                       <div class='project-desc'>"
                     man_html = man_html + "				               <p class='member-bio'>	"+ doc + "</p>"
                     man_html = man_html + "                        </div>"
                     man_html = man_html + "                   </div>"


                     man_html = man_html + "               <div class='project-image col-md-6'> "
                     if(img2 !== '0' || img3 !== '0' ){
                      man_html = man_html + "                   <div class='project-image-carousel'> "
                     }else{
                      man_html = man_html + "                   <div class='project-image'> "	 
                     }	
                     man_html = man_html + "                         <figure> "
                     man_html = man_html + "							   <img src='"+ "https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_blog_imagen.ashx?reg="+ img1 +"' > "
                          man_html = man_html + "                         </figure> "
                     if(img2!== '0'){
                     man_html = man_html + "                         <figure> "
                     man_html = man_html + "							   <img src='"+ "https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_blog_imagen.ashx?reg="+ img2 +"' > "
                          man_html = man_html + "                         </figure> "	
                     }
                     if(img3!== '0'){
                     man_html = man_html + "                         <figure> "
                     man_html = man_html + "							   <img src='"+ "https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_blog_imagen.ashx?reg="+ img3 +"' > "
                          man_html = man_html + "                         </figure> "
                      }
                     man_html = man_html + "                   </div>"
                     man_html = man_html + "              </div>"
                      man_html = man_html + "            </div>"
                     man_html = man_html + "          </div>"
                     man_html = man_html + "    </div>"
                     man_html = man_html + "    </section><!-- member --> "

                      });
                     $('#posts').append(man_html);
                     //HUMILDAD SOY DIOS
                     setTimeout(function(){ portfolioProjectCarousel();  }, 3000);
                     
                     
                     
                
               
                
            },
            error: function (xhr, msg) {
                alert(msg + '\n' + xhr.responseText);
            }
        })
    }


    
	function portfolioProjectCarousel(){
		var projectCarousel = $('.project-image-carousel');

		projectCarousel.owlCarousel({
			singleItem: true,
			items: 1,
			loop: true,
			nav: true,
			autoHeight: true,
			navText: ['<i class="icon-knight-510"></i>', '<i class="icon-knight-522"></i>'],
			animateIn: 'fadeInLeft',
			animateOut: 'fadeOutRight'
		});
	};


   
    

    /*MOSTRAR LOS POST*/
    ShowSingle = function (reg) {
        var URL = "https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_blog.asmx/SinglePost";
        //var URL = "http://localhost:47646/GestionMovil/Servicios/sweb_blog.asmx/SinglePost";
        var man_html = "";
        $('#posts').empty();
        jQuery.ajax({
            type: "POST",
            url: URL,
            data: "empresa=" + "00004" + "&reg="+ reg +"&formato=0116",
            success: function (data) {
                var xml_string = jQuery(data).text();
                var xmlDoc = $.parseXML(xml_string);
                //alert(xml_string);

                var customers = $(xmlDoc).find("Datos");
                $(xml_string).find('Datos').each(function () {
                    doc = $(this).find("TEXTO1").text()
                    tit = $(this).find("TITULO").text()
                    fec = $(this).find("FECHA").text()
                    usu = $(this).find("NOM_USUARIO").text()                    
                    cat = $(this).find("NOMCATEGORIA").text()                    
                    img = $(this).find("REG_ARCHIVO").text()
                    
                    man_html = ""                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
                    
                });
                $('#posts').append(man_html);

            },
            error: function (xhr, msg) {
                alert(msg + '\n' + xhr.responseText);
            }
        })
    }

    CargarPaginas = function () {
        var URL = "https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_blog.asmx/Paginasblog";
        //var URL = "http://localhost:47646/GestionMovil/Servicios/sweb_blog.asmx/Paginasblog";
        var man_elementos = 1;
        jQuery.ajax({
            type: "POST",
            url: URL,
            data: "empresa=" + "00004" + "&cantidad='"+elementos_xpage +"'&npagina=00001",
            success: function (data) {
                var xml_string = jQuery(data).text();
                var xmlDoc = $.parseXML(xml_string);
                var customers = $(xmlDoc).find("Datos");
                $(xml_string).find('Datos').each(function () {
                    cant = $(this).find("CANT").text()
                    paginas = $(this).find("PAGINAS").text()
                    var man_i;
                    var m_cant = parseInt(paginas);
                    var man_html = "<ul>";
                    $('#pagination').empty();
                    //man_html = man_html + "<li class='page-item'><a class='page-link' onclick='previuspage(" + carga_paginador + ")' >ANTERIOR</a></li>";
                    var carga_paginador = 0;

                    /*PAGINA PREVIA EN EL PAGINADOR*/
                    if (pagina_paginador > 1){
                        man_html = man_html + "<li class='pagination-prev'><a class='page-link' onclick='previuspaginator(" + carga_paginador + ")' >...</a></li>";   
                    }
                    console.log("paso 1 {pagina_paginador (" + pagina_paginador +") , m_cant ("+ m_cant +") , pagina_ubicado ("+ pagina_ubicado +") }" );
                    if (pagina_paginador > pagina_post) {
                        pagina_post = pagina_paginador
                    }

                    //ALERT(M_CANT);
                    for (man_i = pagina_paginador; man_i <= m_cant; man_i++) {
                        console.log("paso 2 {man_elementos (" + man_elementos +") , elementos_paginador ("+ elementos_paginador +") , pagina_ubicado ("+ pagina_ubicado +") , pagina_post ("+  pagina_post +") }" );
                        if (man_elementos <= elementos_paginador) {
                            if (man_i == pagina_post) {
                                man_html = man_html + "<li class='active'><a class='page-link' onclick='gopage(" + man_i + ")'>" + man_i + "</a></li>";
                            } else {
                                man_html = man_html + "<li ><a  onclick='gopage(" + man_i + ")' >" + man_i + "</a></li>";
                            }
                            carga_paginador = carga_paginador + 1;
                            console.log("paso 3 {man_i ("+  man_i +") , carga_paginador ("+  carga_paginador +") , pagina_ubicado ("+ pagina_ubicado +")}" );
                            man_elementos = man_elementos + 1;
                            paginas_presentadas = man_i;
                        }
                    }

                    if ((m_cant) > paginas_presentadas)
                    {
                        man_html = man_html + "<li class='pagination-next'><a  onclick='nextpaginator(" + carga_paginador + ")' >...</a></li>";   
                    }
                    //man_html = man_html + "<li class='page-item'><a class='page-link' onclick='nextpage(" + carga_paginador + ")' >SIGUIENTE</a></li>";
                    man_html = man_html + "</ul>"
                    $('#pagination').append(man_html);
                });
            },
            error: function (xhr, msg) {
                alert(msg + '\n' + xhr.responseText);
            }
        })
    }

    /*LLEVA A UNA PAGINA DETERMINADA*/
    gopage = function (page) {
        pagina_post = page;
        CargarPost();
        CargarPaginas()
        $('html, body').animate({ scrollTop: 0 }, 1250);
    }
    /*VA ALA PAGINA SIGUIENTE*/
    nextpage = function (page) {
        pagina_post = pagina_post + 1;
        CargarPost();
        CargarPaginas()
        console.log("nextpage" );
        $('html, body').animate({ scrollTop: 0 }, 1250);        
    }
    /*VA A LA PAGINA PREVIA*/
    previuspage = function (page) {
        pagina_post = pagina_post - 1;
        if (pagina_post <=0 ){
            pagina_post = 1;
        }
        CargarPost();
        CargarPaginas()
        $('html, body').animate({ scrollTop: 0 }, 1250);
    }

    /*PAGINADOR*/
    nextpaginator = function (page) {
        pagina_post = pagina_post + 1;
        pagina_paginador = pagina_paginador + page;
        pagina_ubicado = pagina_ubicado + 1;
        console.log("siguiente" );
        CargarPaginas();
        gopage(pagina_post);
        $('html, body').animate({ scrollTop: 0 }, 1250);
    }
    previuspaginator = function (page) {
        pagina_post = pagina_post + 1;
        pagina_paginador = pagina_paginador - elementos_xprevio;
        pagina_ubicado = pagina_ubicado - 1;
        //console.log("previo" );
        CargarPaginas()
        gopage(paginas_presentadas);
        $('html, body').animate({ scrollTop: 0 }, 1250);
    }
    
    getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
    
            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };


})(jQuery);