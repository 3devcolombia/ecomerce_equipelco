Vue.component('navbartop_info', {
    template: //html
    `
    <div>    
        <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <!-- wasa-->
        <a href="https://api.whatsapp.com/send?phone=573155772974" class="float" target="_blank">
            <i class="fa fa-whatsapp my-float"></i>
        </a>
        

        <div class="mobile-header-wrapper style2 shifter-header blue-bg clearfix hidden-md hidden-lg" style="position:sticky!important;">                   
            <div class="container" style="padding-bottom: 11%;">
                <div class="row" style="
                        background-color: #00457d;
                        width: 124%;
                        margin-left: -12%;
                        height: 50px;
                        padding-top: 5%;
                        margin-bottom: 3%;
                        margin-top: -9%;
                        font-size: 18px;">
                    <div class="col-xs-5">            
                        
                    </div>  
                    <div class="col-xs-2">            
                        <a class="open-popup-link bold" id="logindeusuario" href="#login-form-container" style="color: white;">Login</a>
                        
                    </div>  
                    <div class="col-xs-2">            
                        <a href="profile.html"  style="color: white;">Cuenta</a>
                    </div>    
                    <div class="col-xs-3">            
                        <a onclick="logoutuser()" style="cursor:pointer; color: white;"><span class="fa fa-sign-out" aria-hidden="true"></span></a>
                        <a href="" style="cursor:pointer; color: white;">
                        <span class="basket-icon icon-knight-744"></span>
                        </a>
                    </div> 
                </div>
            </div>
            
            <div class="logo-container">

                    <a href="index.html" class="logo">
                        <img src="img/logo/logo2-transparent.png" alt="Knight Logo">
                    </a>
                </div><!-- /logo-container -->
                <div class="shifter-handle style1">
                    <a href="#" class="bars">
                        <span></span>
                        <span></span>
                        <span></span>
                    </a><!-- /bars -->
                </div><!-- /menu-trigger -->
            </div>
            <!-- /mobile-header-wrapper -->
            <!-- OFF CANVAS MOBILE MENU -->
            <nav class="main-nav offcanvas-menu mobile-nav shifter-navigation fullwidth-items blue-bg">
                <div class="logo-container">
                    <a href="index.html"><img src="img/logo/logo2-transparent.png" alt="Knight Logo"></a>
                </div><!-- /logo-container -->
                <form action="#" class="search-form">
                    <input type="search" name="mobile-search-form" id="mobile-search-form" placeholder="Search">
                    <input type="submit" value="">
                </form>            
            </nav>


    </div>
    `
});