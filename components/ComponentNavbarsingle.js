Vue.component('barrademenusingle', {
    props: ['carrito'],
    template: //html
    `
    <header class="main-header style2 overlay-header">
        <div class="main-header-inner">
            <div class="top-bar style1 blue-bg">
                <div class="container clearfix">
                    <div class="left-sec clearfix">
                        <ul class="contact-info clearfix">
                            <li>
                                <i class="icon icon-knight-284"></i><span>Cel: +57 310 5057 642</span>
                            </li>
                                <li>
                                <i class="icon icon-knight-284"></i><span>Pbx: 4876064</span>
                            </li>
                            <li>
                                <i class="icon icon-knight-365"></i><span>Email: equipelco@equipelco.com</span>
                            </li>
                        </ul>
                    </div>
                    <div class="right-sec clearfix">
                        <ul class="socials style1 clearfix">
                            <li><a href="https://twitter.com/EquipelcoSA" target="_blank" style="color: white ;background-color:#2B3A59;"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://www.facebook.com/Equipelco-SA-355064584582031/"  target="_blank" style="color: white ;background-color:#2B3A59;"> <i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://www.instagram.com/equipelco/?hl=es-la" target="_blank" style="color: white ;background-color:#2B3A59;"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#"  style="color: white ;background-color:#2B3A59;"><i class="fa fa-linkedin"></i></a></li>
                            
                        </ul>
                            <ul class="account-entry style1 clearfix">
                            <li class="account-profile"><a href=""  style="color: white ;background-color:#2B3A59;">My Account</a></li>
                            <li class="wishlist"><a href=""  style="color: white ;background-color:#2B3A59;">My Wishlist</a></li>
                            <li class="account-login"><a class="open-popup-link bold" href="#login-form-container" style="color: white ;background-color:#2B3A59;">Login</a></li>
                            <li id="logout" class="account-login hiddenclass"><a onclick="logoutuser()" style="cursor:pointer; color: white ;background-color:#2B3A59;">LogOut</a></li>
                        
                        </ul>
                    </div>
                </div>
            </div>
            <div class="main-bar padding-37 white-bg">
                <div class="container">
                    <div class="logo-container">
                        <a href="index.html" class="logo">
                            <img src="img/logo/logo4.png" alt="Knight Logo">
                        </a>
                    </div><!-- /logo-container -->
                        <div class="menu-container clearfix">
                        <nav class="main-nav active-style1 style1" id="main-nav">
                            <ul class="clearfix">
                                <li class="dropdown mega-menu full-width active">
                                    <a href="index.html">Inicio</a>
                                    <ul class=" dropdown-menu dark-bg">
                                        <li class="clearfix">
                                            <div class="container">                                                
                                            </div><!-- /container -->
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown mega-menu multi-column">
                                    <a   href="nosotros.html">Nosotros</a>
                                    <ul class="dropdown-menu dark-bg">
                                        <li class="clearfix">                                            
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a  class="dropdown-toggle" data-toggle="dropdown">Servicios</a>
                                    <ul class="sub-menu dropdown-menu blue-bg">
                                        <li><a style="color:white;" href="servicios_industriales.html">Sector Industrial</a></li>
                                        <li><a style="color:white;" href="servicios_publicos.html">Sector Publico</a></li>
                                        <li><a style="color:white;" href="servicios_residenciales.html">Sector Comercial</a></li>
                                    </ul>
                                </li>
                                    
                                
                                <li class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Nuestras marcas</a>
                                    <ul class="sub-menu dropdown-menu blue-bg">
                                        <li class="has-children dropdown">
                                            <a class="dropdown-toggle" style="background: #2b3a59;color:white;" data-toggle="dropdown" href="#">Motores</a>
                                            <ul class="sub-menu dropdown-menu blue-bg">
                                                <li><a style="color:white;" href="catalogo.html?catalogo=MARATHON" >Marathon Motors</a></li>
                                                <li><a style="color:white;"    onclick="cat_usmotors();">us motors</a></li>
                                                <li><a style="color:white;"    onclick="cat_toshiba();">toshiba</a></li>
                                                <li><a style="color:white;"    onclick="cat_motors_abb();">abb</a></li>
                                                <li><a style="color:white;"    onclick="cat_baldor();">baldor</a></li>
                                                <li><a style="color:white;"    onclick="cat_web();">weg</a></li>
                                                <li><a style="color:white;"    onclick="cat_siemens();">siemens</a></li>
                                                
                                            </ul>
                                        </li>
                                        <li class="has-children dropdown">
                                            <a class="dropdown-toggle" style="background: #2b3a59;color:white;padding-right: 30px;" data-toggle="dropdown" href="#">Variadores de velocidad y arrancadores suaves</a>
                                            <ul class="sub-menu dropdown-menu blue-bg">
                                                <li><a style="color:white;"  onclick="cat_abb_variadores();">abb</a></li>
                                            </ul>
                                        </li>
                                        <li class="has-children dropdown">
                                            <a class="dropdown-toggle" style="background: #2b3a59;color:white;" data-toggle="dropdown" href="#">medicion y comprobacion electronica</a>
                                            <ul class="sub-menu dropdown-menu blue-bg">
                                                <li><a style="color:white;" onclick="cat_fluke">fluke</a></li>
                                                <li><a style="color:white;" onclick="cat_flyr">flyr</a></li>
                                                <li><a style="color:white;" onclick="cat_megger">megger</a></li>
                                                <li><a style="color:white;" onclick="car_dranetz">dranetz</a></li>
                                            </ul>
                                        </li>
                                        
                                        <li class="has-children dropdown">
                                            <a class="dropdown-toggle" style="background: #2b3a59;color:white;" data-toggle="dropdown" href="#">conectorizacion</a>
                                            <ul class="sub-menu dropdown-menu blue-bg">
                                                <li><a style="color:white;" onclick="cat_hubbell">hubbell</a></li>
                                                <li><a style="color:white;" onclick="cat_topaz">topaz</a></li>
                                            </ul>
                                        </li>
                                        <li class="has-children dropdown">
                                            <a class="dropdown-toggle" style="background: #2b3a59;color:white;" data-toggle="dropdown" href="#">maniobra</a>
                                            <ul class="sub-menu dropdown-menu blue-bg">
                                                <li><a style="color:white;"  onclick="cat_hubbell_maniobra"      >hubbell</a></li>
                                                <li><a style="color:white;" onclick="cat_topaz_maniobra"    >topaz</a></li>
                                            </ul>
                                        </li>
                                        <li class="has-children dropdown">
                                            <a class="dropdown-toggle" style="background: #2b3a59;color:white;" data-toggle="dropdown" href="#">iluminación led</a>
                                            <ul class="sub-menu dropdown-menu blue-bg">
                                                <li><a style="color:white;" onclick="cat_wlighting">wlighting</a></li>
                                            </ul>
                                        </li>
                                        
                                        <li class="has-children dropdown">
                                            <a class="dropdown-toggle" style="background: #2b3a59;color:white;" data-toggle="dropdown" href="#">sistema fotovoltaico</a>
                                        
                                        </li>
                                        <li class="has-children dropdown">
                                            <a class="dropdown-toggle" style="background: #2b3a59;color:white;" data-toggle="dropdown" href="#">seguridad industrial</a>
                                                <ul class="sub-menu dropdown-menu blue-bg">
                                                    <li><a style="color:white;" onclick="cat_panduit">panduit</a></li>
                                                    <li><a style="color:white;" onclick="cat_schmersal">schmersal</a></li>
                                                </ul>
                                        </li>
                                        <li class="has-children dropdown">
                                            <a class="dropdown-toggle" style="background: #2b3a59;color:white;" data-toggle="dropdown" href="#">herramienta</a>
                                            <ul class="sub-menu dropdown-menu blue-bg">
                                            <li><a style="color:white;" onclick="cat_kleint">kleint tools </a></li>
                                            <li><a style="color:white;" onclick="cat_master">ega master</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown mega-menu full-width">
                                    <a href="tienda.html">tienda</a>
                                </li>
                                        <li class="dropdown mega-menu full-width">
                                    <a  href="casos-exito.html">Casos de exito</a>
                                    <ul class=" dropdown-menu dark-bg">
                                        <li class="clearfix">
                                            <div class="container">                                                        
                                            </div><!-- /container -->
                                        </li>
                                    </ul>
                                </li>
                                    <li class="dropdown mega-menu full-width">
                                    <a  href="noticias.html">Noticias</a>
                                    <ul class=" dropdown-menu dark-bg">
                                        <li class="clearfix">
                                            <div class="container">                                                        
                                            </div><!-- /container -->
                                        </li>
                                    </ul>
                                </li>
                                    <li class="dropdown mega-menu full-width">
                                    <a  href="contacto.html">Contactenos</a>
                                    <ul class=" dropdown-menu dark-bg">
                                        <li class="clearfix">
                                            <div class="container">                                                        
                                            </div><!-- /container -->
                                        </li>
                                    </ul>
                                </li>
                                
                                <li class="icon icon-cart dropdown checkout-style2">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="carrito.html">
                                        <i class="basket-icon icon-knight-744"></i>
                                        <span class="cart-quantity"><span class="items-counter style2">4</span></span>
                                    </a>
                                    <ul class="sub-menu dropdown-menu white-bg">
                                        <li class="clearfix">
                                            <div class="menu-column">                                                           
                                                <div class="product clearfix">
                                                    <figure class="product-preview">
                                                        <img src="img/shop/3_b45340b2-0aa1-4c5e-b114-4cb6f3ecef94.jpg" alt="Product">
                                                    </figure>
                                                    <div class="product-details">
                                                        <div class="product-info">
                                                            <h6 class="heading-alt-style12"><a href="#"> {{carrito}} </a></h6>
                                                            <p class="product-cat"><a href="#">Off-White</a></p>
                                                        </div><!-- /product-info -->
                                                        <div class="product-price">
                                                            <p><span class="quantity">1</span><span class="currency">$</span>300</p>
                                                        </div><!-- /product-price -->
                                                    </div><!-- /product-details -->
                                                    <a href="#" class="product-remove"><i class="icon-knight-521"></i></a>
                                                </div><!-- /product -->
                                                <div class="cart-checkout-buttons-container">
                                                    <div class="checkout-buttons">
                                                        <a @click="vercarrito()" class="button view-cart-button">Ver Carrito</a>
                                                        <a class="button checkout-button">Pagar</a>
                                                    </div><!-- /checkout-buttons -->
                                                </div><!-- /cart-checkout-buttons-container -->
                                            </div><!-- /menu-columns -->
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div><!-- /menu-container -->
                </div><!-- /container -->
            </div><!-- /main-bar -->
        </div><!-- /main-header-inner -->
        <div class="tp-wrapper">
            <div class="tp-banner-container">
                <div class="tp-banner" >
                    <ul>
                        <!-- SLIDE  -->
                        <li data-transition="fade" data-slotamount="7" data-masterspeed="1500">
                            <img src="img/backgrounds/promocionesMes.png"  class="img-responsive"  alt="Knight Background">
                        </li>                                                                            
                    </ul>
                </div><!-- /tp-banner -->
            </div><!-- /tp-banner-container -->
        </div><!-- /tp-wrapper -->
    </header>
    `
});