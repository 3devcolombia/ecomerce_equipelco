Vue.component('navbartop_slider', {
    props: ['carrito', 'cantidadproductos', 'userauth'],
    template: //html
    `
    <div>    
        <header class="main-header style2 fixed-header">
            <div class="main-header-inner">
                <div class="top-bar style1 blue-bg">
                    <div class="container clearfix">
                        <div class="left-sec clearfix">
                            <ul class="contact-info clearfix">
                                <li>
                                    <i class="icon icon-knight-284"></i><span><a href="tel+573155772974" style="color:#fff">Cel: +57 315 5772974</a></span>                                    
                                </li>
                                <li>
                                    <i class="icon icon-knight-284"></i><span>Pbx: 4876064</span>
                                </li>
                                <li>
                                    <i class="icon icon-knight-365"></i><span>Email: equipelco@equipelco.com</span>
                                </li>
                            </ul>
                        </div><!-- /contact-info -->
                        <div class="right-sec clearfix">
                            <ul class="socials style1 clearfix">
                                <li><a href="https://twitter.com/EquipelcoSA" target="_blank"
                                        style="color: white ;background-color:#2B3A59;"><i
                                            class="fa fa-twitter"></i></a></li>
                                <li><a href="https://www.facebook.com/Equipelco-SA-355064584582031/" target="_blank"
                                        style="color: white ;background-color:#2B3A59;"> <i
                                            class="fa fa-facebook"></i></a></li>
                                <li><a href="https://www.instagram.com/equipelco/?hl=es-la" target="_blank"
                                        style="color: white ;background-color:#2B3A59;"><i
                                            class="fa fa-instagram"></i></a></li>
                                <li><a href="#" style="color: white ;background-color:#2B3A59;"><i
                                            class="fa fa-linkedin"></i></a></li>
                            </ul>
                            <ul class="account-entry style1 clearfix">
                                <li v-if="userauth !== 'false'" id="profileclass" class="account-profile">
                                    <a href="profile.html" style="color: #fff!important;">
                                        <i class="icon icon2-2x icon-knight-343" style="vertical-align: middle;"></i>
                                        <span v-if="userauth">{{userauth}}</span>
                                    </a>
                                </li>                            
                                
                                <li class="account-login"><a class="open-popup-link bold" id="logindeusuario" href="#login-form-container" style="color: white ;background-color:#2B3A59;">Login</a></li>
                                <li v-if="userauth !== 'false'" id="logout" class="account-login"><a onclick="logoutuser()" style="cursor:pointer; color: white ;background-color:#2B3A59;">LogOut</a></li>
                            </ul>
                        </div>
                    </div><!-- /info -->
                </div><!-- /top-bar -->
                <div class="main-bar padding-37 white-bg">


                    <a class="open-popup-link bold" id="bienvenidoid" href="#login-form-bienvenido" style="display: none;">bienvenido</a>


                           <div class="container">
                            <div class="row">
                            <div class="logo-container">
                                <a href="index.html" class="logo">
                                    <img src="img/logo/logo4.png" alt="Knight Logo">
                                </a>
                            </div><!-- /logo-container -->
                            <div class="menu-container clearfix">
                                <nav class="main-nav active-style1 style1" id="main-nav">
                                    <ul class="clearfix">
                                        <li class="dropdown mega-menu full-width active">
                                            <a href="index.html">Inicio</a>
                                            <ul class=" dropdown-menu dark-bg">
                                                <li class="clearfix">
                                                    <div class="container">

                                                    </div><!-- /container -->
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="dropdown mega-menu multi-column">
                                            <a href="nosotros.html">Nosotros</a>
                                            <ul class="dropdown-menu dark-bg">
                                                <li class="clearfix">

                                                </li>
                                            </ul>
                                        </li>
                                        <li class="dropdown">
                                            <a style="cursor:pointer;" class="dropdown-toggle" data-toggle="dropdown">Servicios</a>
                                            <ul class="sub-menu dropdown-menu blue-bg">
                                                <li>
                                                    <a style="color:white;" href="servicios_industriales.html">Sector Industrial</a></li>
                                                <li>
                                                    <a style="color:white;" href="servicios_publicos.html">Sector Publico</a></li>
                                                <li>
                                                    <a style="color:white;" href="servicios_residenciales.html">Sector Comercial</a>
                                                </li>
                                            </ul>
                                        </li>    
                                        <li class="dropdown">
                                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Nuestras marcas</a>
                                                <ul class="sub-menu dropdown-menu blue-bg">
                                                    <li class="has-children dropdown">
                                                        <a class="dropdown-toggle" style="background: #2b3a59;color:white;"
                                                            data-toggle="dropdown" href="#">Motores</a>
                                                        <ul class="sub-menu dropdown-menu blue-bg">
                                                        <li><a style="color:white;" href="catalogo.html?catalogo=MARATHON">Marathon Motors</a></li>
                                                        <li><a style="color:white;" href="catalogo.html?catalogo=ABB">Abb</a></li>
                                                        <li><a style="color:white;" href="catalogo.html?catalogo=BALDOR">Baldor</a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="has-children dropdown">
                                                        <a class="dropdown-toggle"
                                                            style="background: #2b3a59;color:white;padding-right: 30px;"
                                                            data-toggle="dropdown" href="#">Variadores de velocidad y arrancadores
                                                            suaves</a>
                                                        <ul class="sub-menu dropdown-menu blue-bg">
                                                            <li><a style="color:white;" href="catalogo.html?catalogo=VAR_ABB">Abb</a></li>
                                                            
                                                        </ul>
                                                    </li>
                                                    <li class="has-children dropdown">
                                                        <a class="dropdown-toggle" style="background: #2b3a59;color:white;"
                                                            data-toggle="dropdown" href="#">medicion y comprobacion electronica</a>
                                                        <ul class="sub-menu dropdown-menu blue-bg">
                                                        <li><a style="color:white;" href="catalogo.html?catalogo=FLUKE">Fluke</a></li>
                                                            
                                                        </ul>
                                                    </li>
            
                                                    <li class="has-children dropdown">
                                                        <a class="dropdown-toggle" style="background: #2b3a59;color:white;"
                                                            data-toggle="dropdown" href="#">conectorizacion</a>
                                                        <ul class="sub-menu dropdown-menu blue-bg">
                                                                <li><a style="color:white;" href="catalogo.html?catalogo=HUBELL">Hubbell</a></li>
                                                                <li><a style="color:white;" href="catalogo.html?catalogo=TOPAZ">Topaz</a></li>
                                                            
                                                        </ul>
                                                    </li>
                                                    <li class="has-children dropdown">
                                                        <a class="dropdown-toggle" style="background: #2b3a59;color:white;"
                                                            data-toggle="dropdown" href="#">maniobra</a>
                                                        <ul class="sub-menu dropdown-menu blue-bg">
            
                                                        <li><a style="color:white;" href="catalogo.html?catalogo=ABB_MANIOBRA">Abb</a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="has-children dropdown">
                                                        <a class="dropdown-toggle" style="background: #2b3a59;color:white;"
                                                            data-toggle="dropdown" href="#">iluminaci�n led</a>
                                                        <ul class="sub-menu dropdown-menu blue-bg">
                                                                <li><a style="color:white;" href="catalogo.html?catalogo=WLIGHTING">Wlighting</a></li>                                                
                                                        </ul>
                                                    </li>
            
                                                    <li class="has-children dropdown">
                                                        <a class="dropdown-toggle" style="background: #2b3a59;color:white;"
                                                            data-toggle="dropdown" href="#">sistema fotovoltaico</a>
            
                                                    </li>
                                                    <li class="has-children dropdown">
                                                        <a class="dropdown-toggle" style="background: #2b3a59;color:white;"
                                                            data-toggle="dropdown" href="#">seguridad industrial</a>
                                                        <ul class="sub-menu dropdown-menu blue-bg">
                                                            <li><a style="color:white;" href="catalogo.html?catalogo=PANDUIT">Panduit</a></li> 
                                                            <li><a style="color:white;" href="catalogo.html?catalogo=SCHMERSAL">Schmersal</a></li> 
                                                            
                                                        </ul>
                                                    </li>
                                                    <li class="has-children dropdown">
                                                        <a class="dropdown-toggle" style="background: #2b3a59;color:white;"
                                                            data-toggle="dropdown" href="#">herramienta</a>
                                                        <ul class="sub-menu dropdown-menu blue-bg">
                                                            <li><a style="color:white;" href="catalogo.html?catalogo=KLEINT_TOOLS">Kleint tools</a></li> 
                                                            <li><a style="color:white;" href="catalogo.html?catalogo=EGA_MASTER">Ega Master</a></li> 
                                                            
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                        <li class="dropdown mega-menu full-width">
                                            <a href="tienda.html">tienda</a>
                                        </li>
                                        <li class="dropdown mega-menu full-width">
                                            <a href="casos-exito.html">Casos de exito</a>
                                            <ul class=" dropdown-menu dark-bg">
                                                <li class="clearfix">
                                                    <div class="container">
                                                    </div><!-- /container -->
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="dropdown mega-menu full-width">
                                            <a href="noticias.html">Noticias</a>
                                            <ul class=" dropdown-menu dark-bg">
                                                <li class="clearfix">
                                                    <div class="container">
                                                    </div><!-- /container -->
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="dropdown mega-menu full-width">
                                            <a href="contacto.html">Contactenos</a>                                            
                                            <ul class=" dropdown-menu dark-bg">
                                                <li class="clearfix">
                                                    <div class="container">
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>                                        

                                        <li class="icon icon-cart dropdown checkout-style2">
                                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                <i class="basket-icon icon-knight-744"></i>
                                                <span class="cart-quantity"><span class="items-counter style2">{{cantidadproductos}}</span></span>                                                
                                            </a>                                            
                                            <ul class="sub-menu dropdown-menu white-bg">                                            
                                                <li class="clearfix">
                                                    <div class="menu-column">  
                                                        <div class="product clearfix" v-for="cart in carrito">
                                                            <figure class="product-preview">
                                                                <img :src="'https://3dev.com.co/Doom/Bootstrap/ShowImage.ashx?ID='+cart.REFERENCIA+'&TIPO=FRENTE&COLOR=*&EMPRESA=00004'" alt="Producto Equipelco">
                                                            </figure>
                                                            <div class="product-details">
                                                                <div class="product-info">
                                                                    <h6 class="heading-alt-style12"><a href="#">{{ cart.REG }}</a></h6>
                                                                    <p class="product-cat"><a href="#">{{ cart.REFERENCIA }}</a></p>
                                                                </div><!-- /product-info -->
                                                                <div class="product-price">
                                                                    <p><span class="quantity">{{ cart.CANTIDAD }}</span><span class="currency">$</span>{{ cart.VALOR }}</p>
                                                                </div><!-- /product-price -->
                                                            </div><!-- /product-details -->
                                                            <a @click="deleteitem(cart.REFERENCIA, cart.REG, cart)" class="product-remove"><i class="icon-knight-521"></i></a>
                                                        </div><!-- /product -->

                                                        <div class="cart-checkout-buttons-container">
                                                            <div class="checkout-buttons">
                                                                <a href="carrito.html" class="button view-cart-button">Ver Carrito</a>
                                                            </div><!-- /checkout-buttons -->
                                                        </div><!-- /cart-checkout-buttons-container -->
                                                    </div><!-- /menu-columns -->
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </nav>
                            </div><!-- /menu-container -->
                            </div>
                        
                    </div><!-- /container -->
                </div><!-- /main-bar -->
            </div><!-- /main-header-inner -->
        </header>
       
    </div>
    `,

    data() {
        return {

        }
      },
    methods: {   
        deleteitem: function(registro, reg, todoitem){
            this.carrito.splice(this.carrito.indexOf(todoitem), 1);                        
            var storedlogin = JSON.parse(localStorage.getItem("loginusuario"));                        

            var delitem = ""
            delitem = delitem + "<PagXml>                                                          ";
            delitem = delitem + "   <CorXml>                                                       ";
            delitem = delitem + "      <TIPO>PARAMETRO</TIPO>                                      ";
            delitem = delitem + "      <PROCESO>0147</PROCESO>                                     "; 
            delitem = delitem + "      <SUCURSAL>00004</SUCURSAL>                                  ";
            delitem = delitem + "      <TIPODOC>PEW</TIPODOC>                                      ";
            delitem = delitem + "      <DOCUMENTO>@@TT1@@</DOCUMENTO>                              ";      
            delitem = delitem + "      <USERID>"+storedlogin.CORREO+"</USERID>                     ";
            delitem = delitem + "      <PASSWORD>"+storedlogin.CLAVE+"</PASSWORD>                  ";         
            delitem = delitem + "      <REG>"+reg+"</REG>                                          "; 
            delitem = delitem + "   </CorXml>                                                      ";
            delitem = delitem + "</PagXml>                                                         ";
            axios({
                method: 'post',
                url: 'https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_carrito.asmx/SetPedido',
                data: "Parametros=" + delitem + "",
                config: { headers: {'Content-Type': 'multipart/form-data' }}
            })
            .then(response => {
            })
            .catch(error => {
                console.log(error.response);
            });
        },
    },
});