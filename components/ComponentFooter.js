Vue.component('footerweb', {
    template: //html
    `
    <footer class="main-footer padding-top112 sm-padding-top80 dark-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="row">
                        <div class="col-md-12 col-sm-6">
                            <div class="empty-space38 sm-empty-space0"></div>
                            <div class="widget widget-about">
                                <a class="logo padding-bottom37" href="index.html"><img
                                        src="img/logo/logo2-transparent.png" alt="Knight Logo"></a>
                            </div><!-- /widget-about -->
                        </div><!-- /col-sm-6 -->
                        <div class="col-md-12 col-sm-6">
                            <div class="widget widget-contact-info widget-contact-info-style1">
                                <h6 style="color: #fff !important;"class="widget-title heading-alt-style8 short-underline">Informacion de contacto
                                </h6>
                                <address style="color: #fff;">
                                    <p class="clearfix">
                                        <i class="icon icon-knight-220" style="color: gray;"></i><span>Cl. 23 #5-30, Cali, Valle del
                                            Cauca</span>
                                    </p>

                                    <p class="clearfix">
                                        <i  class="icon icon-knight-284" style="color: gray;"></i><span><a style="color: #fff !important" href="tel+573155772974">Tel: +57 315 5772974</a></span>                                        
                                    </p>
                                    <p class="clearfix">
                                        <i class="icon icon-knight-302" style="color: gray;"></i><span>Pbx: 4876064</span>
                                    </p>
                                    <p class="clearfix">
                                        <i class="icon icon-knight-365" style="color: gray;"></i><span>EMAIL:
                                            equipelco@equipelco.com</span>
                                    </p>
                                    <p class="clearfix">
                                        <i class="icon large-icon icon-knight-283" style="color: gray;"></i><span>WEB: <a style="color: #fff !important"
                                                class="underline"
                                                href="http://equipelco.com/">www.equipelco.com</a></span>
                                    </p>
                                </address>
                            </div><!-- /widget-contact-info -->
                        </div><!-- /col-sm-6 -->
                    </div><!-- /row -->
                </div><!-- /col-md-3 -->
                <div class="empty-space35 sm-empty-space35"></div>
                <div class="col-md-8 col-md-offset-1">
                    <div class="widget widget-contact-form widget-contact-form-style1">
                        <h6 style="color: #fff !important" class="widget-title heading-alt-style8 short-underline">Formulario de contacto</h6>
                        <form class="contact-form">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <input type="text" class=" contact-form-name" name="name"
                                            id="register-form-name" placeholder="Nombre completo"
                                            style="background-color: #222222;color: #545454;" required>
                                    </div>
                                    <div class="col-md-4">
                                        <input type="email" class="contact-form-email" name="email"
                                            id="register-form-email" placeholder="Correo"
                                            style="background-color: #222222;color: #545454; padding-right: 60px;"
                                            required>
                                    </div>
                                    <div class="col-md-4">
                                        <input type="phone" class="contact-form-phone" name="Telefono"
                                            id="register-form-phone" placeholder="Telefono"
                                            style="background-color: #222222;color: #545454;padding-right: 35px;"
                                            required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <textarea class="contact-form-message" name="message" cols="90" rows="10"
                                        id="register-form-requiere" placeholder="Descripcion"
                                        style="background-color: #222222;color: #545454;" required></textarea>
                                </div>
                                <div class="col-md-2">
                                    <button onclick="ReciveEnvioContacto();"
                                        class="btn btn-primary button-black nomargin" id="register-form-submit"
                                        name="register-form-submit" value="register">Enviar mensaje</button>
                                </div>

                                <!-- <input type="submit" class="form-submit" name="form-submit" value="Enviar mensaje"> -->
                            </div><!-- /row -->
                        </form>
                        <div class="contact-form-message"></div>
                    </div><!-- /widget-contact-form -->
                </div><!-- /col-md-8 -->
            </div><!-- /row -->
        </div><!-- /container -->
        <div class="footer-bottom-bar margin-top120 sm-margin-top65 clearfix">
            <div class="container">
                <div class="socials-container">
                    <p class="uppercase">Redes Sociales:</p>
                    <div class="widget widget-socials widget-socials-big">
                        <ul class="clearfix">
                            <li><a href="https://twitter.com/EquipelcoSA" target="_blank"><i
                                        class="fa fa-twitter"></i></a></li>
                            <li><a href="https://www.facebook.com/Equipelco-SA-355064584582031/" target="_blank"> <i
                                        class="fa fa-facebook"></i></a></li>
                            <li><a href="https://www.instagram.com/equipelco/?hl=es-la" target="_blank"><i
                                        class="fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div><!-- /widget-socials -->
                </div><!-- /socials-container -->
                <p class="copyright uppercase">© <strong id="fecha"></strong> todos los derechos reservados
                    Equipelco S.A.S</p>
            </div><!-- /container -->
        </div><!-- /footer-bottom-bar -->
    </footer>
    `
});
