//====== Deslogear usuario
function logoutuser(){                    
    localStorage.removeItem("loginusuario");
    location.reload();
}


new Vue({
    el: '#app',
    data () {
        return {
            carrito: [],
            ciudades: [],
            departamentos: [],
            logeado:[],
            cityuser: [],
            infos: [],
            statuspedidos: [],
            paginate: ['infos'],
            updatecantidad: [],
            cantidadinput: "",
            transaccEstado: '',
            marcas: [],
            vacio:"false",
            grupos: [],
            userAuth: [],
            gruposelected: "",
            marcaselected: "",
            totalcarrito: 0,
            md5cadena: '',
            referenciacodigo:'00000000',                            
            cantidadproductos: 0,
            fnameuser: "",
            lnameuser: "",
            identificacion: "",
            emailuser: "",
            teluser: "",
            addressuser: "",                            
            regionuser: "",
            companyuser: "",
            postcodeuser: "",
            passuser: "",
            confirmpassuser: "",
            inputloginuser: "",
            loginpass: "",
            manualpaginate: 15,
            name: "",
            genero: "",
            price: "",
            ref: "",
            marca: "",
            design: "",
            group: "",
            tiendagrilla: 0,
            Filtro: "",
            cantidadprotienda: 1,
            banderapedido: 0,
        }      
    },


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



    computed: {
        cantidadproducto() {
            return this.cantidadinput
        }
    },


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



    methods: { 

        //DAR FORMATO A LOS NUMEROS
        formatPrice(value) {
            let val = (value/1).toFixed(2).replace(',', '.')
            return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        },
        // =============== TIENDA cambiar tipo de grilla
        tipogrilla(tipo){
            var self = this;
            self.tiendagrilla = tipo;
            if (self.tiendagrilla == 0) {
                console.log(self.tiendagrilla);
                
            } else if(self.tiendagrilla == 1) {
                console.log(self.tiendagrilla);
            }

        },

        //========================= FILTROS MARCAS Y GRUPOS ===================
        categoriasgrupo: function(gruposvalor){
            
            //==== GRUPOS ===
            this.gruposelected = gruposvalor;
            
            var self = this;
            this.Filtro = "SUCURSAL= 00004, GRUPO = "+this.gruposelected+"";
            
            
            //==================== MOSTRAR TODOS LOS PRODUCTOS POR GRUPO EN LA TIENDA ====================  
            var consulta = ""
                consulta = consulta + "<PagXml>                                                     ";
                consulta = consulta + "   <CorXml>                                                  ";
                consulta = consulta + "      <TIPO>PARAMETRO</TIPO>                                 ";
                consulta = consulta + "      <CODIGO>0099</CODIGO>                                  ";
                consulta = consulta + "      <PAGINA>1</PAGINA>                                     ";
                consulta = consulta + "      <PAGINAS>20000</PAGINAS>                               ";
                consulta = consulta + "      <NOMBRE>PROCESO</NOMBRE>                               ";
                consulta = consulta + "      <ORDEN>1</ORDEN>                                       ";
                consulta = consulta + "      <VALOR><![CDATA[EMPRESA.CONTACTOS]]></VALOR>           ";
                consulta = consulta + "      <FILTRO><![CDATA[" + this.Filtro + "]]></FILTRO>       ";
                consulta = consulta + "   </CorXml>                                                 ";
                consulta = consulta + "</PagXml>                                                    ";  
                
            axios
            .get('https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_carrito.asmx/FiltroBusquedaJson', {
                params: {
                    Parametros: consulta 
                }
            })
            .then( function(response){                           
                var cadenainicio = response.data,        
                inicio = 90,
                subCadenainicio = cadenainicio.substring(inicio);                              
                var cadena = subCadenainicio,
                patron = "}</string>",
                nuevoValor    = "",
                nuevaCadena = cadena.replace(patron, nuevoValor);            
                var filtrogrupos = JSON.parse(nuevaCadena);                    
                
                //VALIDACION IMPORTANTE llega 1 producto o un grupo de ellos.
                if (filtrogrupos.Estado.Registros >= 2) {
                    self.infos = filtrogrupos.Datos;
                    
                } else if(filtrogrupos.Estado.Registros == 1){
                    var found = Object.values(filtrogrupos);
                    self.infos = found.slice(0,1);             
                }   
            })                         
        },
        //====MARCAS ===
        categoriasmarca: function(marcasvalor){
            this.marcaselected = marcasvalor;

            var self = this;
            
            this.Filtro = "SUCURSAL= 00004, MARCA = "+this.marcaselected+"";
            
            //==================== MOSTRAR TODOS LOS PRODUCTOS POR MARCA EN LA TIENDA ====================  
            var consulta = ""
                consulta = consulta + "<PagXml>                                                     ";
                consulta = consulta + "   <CorXml>                                                  ";
                consulta = consulta + "      <TIPO>PARAMETRO</TIPO>                                 ";
                consulta = consulta + "      <CODIGO>0099</CODIGO>                                  ";
                consulta = consulta + "      <PAGINA>1</PAGINA>                                     ";
                consulta = consulta + "      <PAGINAS>20000</PAGINAS>                               ";
                consulta = consulta + "      <NOMBRE>PROCESO</NOMBRE>                               ";
                consulta = consulta + "      <ORDEN>1</ORDEN>                                       ";
                consulta = consulta + "      <VALOR><![CDATA[EMPRESA.CONTACTOS]]></VALOR>           ";
                consulta = consulta + "      <FILTRO><![CDATA[" + this.Filtro + "]]></FILTRO>       ";
                consulta = consulta + "   </CorXml>                                                 ";
                consulta = consulta + "</PagXml>                                                    ";  
                
            axios
            .get('https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_carrito.asmx/FiltroBusquedaJson', {
                params: {
                    Parametros: consulta 
                }
            })
            .then( function(response){                           
                var cadenainicio = response.data,        
                inicio = 90,
                subCadenainicio = cadenainicio.substring(inicio);                              
                var cadena = subCadenainicio,
                patron = "}</string>",
                nuevoValor    = "",
                nuevaCadena = cadena.replace(patron, nuevoValor);            
                var filtromarcas = JSON.parse(nuevaCadena);                    
                
                //VALIDACION IMPORTANTE llega 1 producto o un grupo de ellos.
                if (filtromarcas.Estado.Registros >= 2) {
                    self.infos = filtromarcas.Datos;
                    
                } else if(filtromarcas.Estado.Registros == 1){
                    var found = Object.values(filtromarcas);
                    self.infos = found.slice(0,1);             
                } 
                    
            }) 
        },        
        //========================= FIN  FILTROS MARCAS Y GRUPOS ===================    




        //==================== REGISTRAR USUARIOS
        registeruser: function(){
            var registrarusuario = ""
            registrarusuario = registrarusuario + "<PagXml>                                                          ";
            registrarusuario = registrarusuario + "   <CorXml>                                                       ";
            registrarusuario = registrarusuario + "      <TIPO>PARAMETRO</TIPO>                                      ";
            registrarusuario = registrarusuario + "      <PROCESO>0141</PROCESO>                                     "; //registro de usuario
            registrarusuario = registrarusuario + "      <SUCURSAL>00004</SUCURSAL>                                  ";
            registrarusuario = registrarusuario + "      <NOMBRES>"+this.fnameuser+"</NOMBRES>                       ";
            registrarusuario = registrarusuario + "      <APELLIDOS>"+this.lnameuser+"</APELLIDOS>                   ";
            registrarusuario = registrarusuario + "      <IDENTIFICACION>"+this.identificacion+"</IDENTIFICACION>    ";
            registrarusuario = registrarusuario + "      <CORREO>"+this.emailuser+"</CORREO>                         ";
            registrarusuario = registrarusuario + "      <TELEFONO>"+this.teluser+"</TELEFONO>                       ";
            registrarusuario = registrarusuario + "      <DIRECCION>"+this.addressuser+"</DIRECCION>                 ";
            registrarusuario = registrarusuario + "      <DEPARTAMENTO>"+this.regionuser+"</DEPARTAMENTO>            ";
            registrarusuario = registrarusuario + "      <CIUDAD>"+this.cityuser+"</CIUDAD>                          ";
            registrarusuario = registrarusuario + "      <EMPRESA>"+this.companyuser+"</EMPRESA>                     ";
            registrarusuario = registrarusuario + "      <CODIGO_POSTAL>"+this.postcodeuser+"</CODIGO_POSTAL>        ";
            registrarusuario = registrarusuario + "      <FLAG_FACTURA>1</FLAG_FACTURA>                              ";
            registrarusuario = registrarusuario + "      <CLAVE>"+this.passuser+"</CLAVE>                            ";
            registrarusuario = registrarusuario + "      <REG>0</REG>                                                "; //si llega registro actualia       
            registrarusuario = registrarusuario + "      <VALOR><![CDATA[EMPRESA.CONTACTOS]]></VALOR>                ";
            registrarusuario = registrarusuario + "   </CorXml>                                                      ";
            registrarusuario = registrarusuario + "</PagXml>                                                         ";

            axios({
                method: 'post',
                url: 'https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_carrito.asmx/SetData',
                data: "Parametros=" + registrarusuario + "",
                config: { headers: {'Content-Type': 'multipart/form-data' }}
            })
            .then(response => {
                document.getElementById("bienvenidoid").click();


            })
            .catch(error => {
                //handle error
                console.log(error.response);
            });
        },


        //======================= USER LOGIN
        loginuser: function(){                            
            var loginusers = ""
                loginusers = loginusers + "<PagXml>                                         ";
                loginusers = loginusers + "   <CorXml>                                      ";
                loginusers = loginusers + "      <TIPO>PARAMETRO</TIPO>                     ";
                loginusers = loginusers + "      <PROCESO>0142</PROCESO>                    ";
                loginusers = loginusers + "      <SUCURSAL>00004</SUCURSAL>                ";
                loginusers = loginusers + "      <CORREO>"+this.inputloginuser+"</CORREO>       ";
                loginusers = loginusers + "      <CLAVE>"+this.loginpass+"</CLAVE>          ";
                loginusers = loginusers + "   </CorXml>                             ";
                loginusers = loginusers + "</PagXml>                               ";                                                        

            axios({
                method: 'post',
                url: 'https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_carrito.asmx/SetData',
                data: "Parametros=" + loginusers + "",
            })
            .then(function (response) {
                var cadenainicio = response.data,
                inicio = 90,
                subCadenainicio = cadenainicio.substring(inicio);  
                
                var cadena = subCadenainicio,
                patron = "}</string>",
                nuevoValor = "",
                nuevaCadena = cadena.replace(patron, nuevoValor);
                var aux = JSON.parse(nuevaCadena);
                self.logeado = aux.Datos;

                localStorage.setItem('loginusuario', JSON.stringify(this.logeado));
                
                location.reload();
            })
            .catch(function (error) {
                console.log(error);
            });
        },

        // ================ DETALLES DEL PRODUCTO
        cargarpopup(nombre, genero, price, ref, marca, design, group ) {
            //alert(nombre);
            this.name = nombre;
            this.genero = genero;
            this.price = price;
            this.ref = ref;
            this.marca = marca;
            this.design = design;
            this.group = group;
        },


        // ================== UPDATE CANTIDAD PRODUCTO DEL CARRITO (PEDIDO DEL USUARIO)
        updateqtycarrito(cantidad, reg, referencia) {
            // console.log("cantidad: "+cantidad+" codigo producto: "+reg)
            var storedlogin = JSON.parse(localStorage.getItem("loginusuario")); 
            
            if (storedlogin !== null) {
                var updatepedido = ""
                updatepedido = updatepedido + "<PagXml>                                                         ";
                updatepedido = updatepedido + "   <CorXml>                                                      ";
                updatepedido = updatepedido + "      <TIPO>PARAMETRO</TIPO>                                     ";
                updatepedido = updatepedido + "      <PROCESO>0146</PROCESO>                                    "; 
                updatepedido = updatepedido + "      <SUCURSAL>00004</SUCURSAL>                                 ";
                updatepedido = updatepedido + "      <TIPODOC>PEW</TIPODOC>                                     ";  //FIJO     
                updatepedido = updatepedido + "      <DOCUMENTO>@@TT1@@</DOCUMENTO>                             ";  //FIJO     
                updatepedido = updatepedido + "      <USERID>"+storedlogin.CORREO+"</USERID>                    ";
                updatepedido = updatepedido + "      <PASSWORD>"+storedlogin.CLAVE+"</PASSWORD>                 ";      
                updatepedido = updatepedido + "      <ITEMS>"+referencia+"</ITEMS>                              ";
                updatepedido = updatepedido + "      <COLOR>*</COLOR>                                           ";
                updatepedido = updatepedido + "      <TALLA>*</TALLA>                                           ";
                updatepedido = updatepedido + "      <CANTIDAD>"+cantidad+"</CANTIDAD>                          ";
                updatepedido = updatepedido + "      <REG>"+reg+"</REG>                                         "; //si llega registro actualia       
                updatepedido = updatepedido + "   </CorXml>                                                     ";
                updatepedido = updatepedido + "</PagXml>                                                        ";
                axios({
                    method: 'post',
                    url: 'https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_carrito.asmx/SetPedido',
                    data: "Parametros=" + updatepedido + "",
                    config: { headers: {'Content-Type': 'multipart/form-data' }}
                })
                .then(response => {
                    var cadenainicio = response.data,        
                    inicio = 90,
                    subCadenainicio = cadenainicio.substring(inicio);                              
                    var cadena = subCadenainicio,
                    patron = "}</string>",
                    nuevoValor    = "",
                    nuevaCadena = cadena.replace(patron, nuevoValor);            
                    var pedidocarrito = JSON.parse(nuevaCadena);                    
                    
                    //VALIDACION IMPORTANTE llega 1 producto o un grupo de ellos.
                    if (pedidocarrito.Estado.Registros >= 2) {
                        this.carrito = pedidocarrito.Datos;
                        this.referenciacodigo = this.carrito[0].DOCUMENTO;

                        //suma total de los productos dentro del carrito
                        var totales = 0;
                        var valores = 0;
                        var contadorproductos = 0;

                        this.carrito.forEach(element => {                           
                            valores = parseInt(element.VALOR, 10);
                            totales = totales + valores;
                            contadorproductos = contadorproductos + 1;
                        });
                        this.totalcarrito = formatNumber.new(totales.toFixed(2));
                        this.cantidadproductos = contadorproductos;

                        //crear la firma en base al codigo de referencia del pedido 
                        md5string = hex_md5("4Vj8eK4rloUd272L48hsrarnUA~508029~"+this.referenciacodigo+"~"+this.totalcarrito+"~COP");
                        this.md5cadena = md5string;

                        
                    } else if(pedidocarrito.Estado.Registros == 1){
                        var found = Object.values(pedidocarrito);
                        this.carrito = found.slice(0,1);
                        
                        this.referenciacodigo = this.carrito[0].DOCUMENTO;

                        var totales = 0;
                        var valores = 0;
                        var contadorproductos = 1;
                        this.cantidadproductos = contadorproductos;

                        totales = parseInt(this.carrito[0].VALOR, 10);

                        valores = totales.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                        this.totalcarrito = valores;

                        //crear la firma en base al codigo de referencia del pedido
                        md5string = hex_md5("4Vj8eK4rloUd272L48hsrarnUA~508029~"+this.referenciacodigo+"~"+this.totalcarrito+"~COP");
                        this.md5cadena = md5string;  
                    } 
                })
                .catch(error => {
                    console.log(error.response);
                });                        
            }else{
                document.getElementById("logindeusuario").click();
                console.log("no estas logueado, logeate o registrate"); 
            }
        },



        // ================== AGREGAR PRODUCTOS AL CARRITO (PEDIDO DEL USUARIO)
        addcarrito(cantidad, nombre, genero, price, ref, marca, design, group) {
            var storedlogin = JSON.parse(localStorage.getItem("loginusuario"));             

            var cantidadtienda = 0;

            //validar cantidad del popup tienda cantidad
            if (cantidad == "") {
                var cantidadtienda = 1;
            }else{
                var cantidadtienda = cantidad;
            }

            if (storedlogin !== null) {
                var pedido = ""
                pedido = pedido + "<PagXml>                                                         ";
                pedido = pedido + "   <CorXml>                                                      ";
                pedido = pedido + "      <TIPO>PARAMETRO</TIPO>                                     ";
                pedido = pedido + "      <PROCESO>0146</PROCESO>                                    "; 
                pedido = pedido + "      <SUCURSAL>00004</SUCURSAL>                                 ";
                pedido = pedido + "      <TIPODOC>PEW</TIPODOC>                                     ";  //FIJO     
                pedido = pedido + "      <DOCUMENTO>@@TT1@@</DOCUMENTO>                             ";  //FIJO     
                pedido = pedido + "      <USERID>"+storedlogin.CORREO+"</USERID>                    ";
                pedido = pedido + "      <PASSWORD>"+storedlogin.CLAVE+"</PASSWORD>                 ";      
                pedido = pedido + "      <ITEMS>"+ref+"</ITEMS>                                     ";
                pedido = pedido + "      <COLOR>*</COLOR>                                           ";
                pedido = pedido + "      <TALLA>*</TALLA>                                           ";
                pedido = pedido + "      <CANTIDAD>"+cantidadtienda+"</CANTIDAD>                                     ";
                pedido = pedido + "      <REG>0</REG>                                               "; //si llega registro actualia       
                pedido = pedido + "   </CorXml>                                                     ";
                pedido = pedido + "</PagXml>                                                        ";
                axios({
                    method: 'post',
                    url: 'https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_carrito.asmx/SetPedido',
                    data: "Parametros=" + pedido + "",
                    config: { headers: {'Content-Type': 'multipart/form-data' }}
                })
                .then(response => {
                    var cadenainicio = response.data,        
                    inicio = 90,
                    subCadenainicio = cadenainicio.substring(inicio);                              
                    var cadena = subCadenainicio,
                    patron = "}</string>",
                    nuevoValor    = "",
                    nuevaCadena = cadena.replace(patron, nuevoValor);            
                    var pedidocarrito = JSON.parse(nuevaCadena);                    
                    
                    //VALIDACION IMPORTANTE llega 1 producto o un grupo de ellos.
                    if (pedidocarrito.Estado.Registros >= 2) {
                        this.carrito = pedidocarrito.Datos;
                        this.referenciacodigo = this.carrito[0].DOCUMENTO;

                        //suma total de los productos dentro del carrito
                        var totales = 0;
                        var valores = 0;
                        var contadorproductos = 0;

                        this.carrito.forEach(element => {                           
                            valores = parseInt(element.VALOR, 10);
                            totales = totales + valores;
                            contadorproductos = contadorproductos + 1;
                        });
                        this.totalcarrito = formatNumber.new(totales.toFixed(2));
                        this.cantidadproductos = contadorproductos;

                        //crear la firma en base al codigo de referencia del pedido 
                        md5string = hex_md5("4Vj8eK4rloUd272L48hsrarnUA~508029~"+this.referenciacodigo+"~"+this.totalcarrito+"~COP");
                        this.md5cadena = md5string;

                        
                    } else if(pedidocarrito.Estado.Registros == 1){
                        var found = Object.values(pedidocarrito);
                        this.carrito = found.slice(0,1);
                        
                        this.referenciacodigo = this.carrito[0].DOCUMENTO;

                        var totales = 0;
                        var valores = 0;
                        var contadorproductos = 1;
                        this.cantidadproductos = contadorproductos;

                        totales = parseInt(this.carrito[0].VALOR, 10);

                        valores = totales.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                        this.totalcarrito = valores;

                        //crear la firma en base al codigo de referencia del pedido
                        md5string = hex_md5("4Vj8eK4rloUd272L48hsrarnUA~508029~"+this.referenciacodigo+"~"+this.totalcarrito+"~COP");
                        this.md5cadena = md5string;  
                    } 
                
                })
                .catch(error => {
                    console.log(error.response);
                });                        
            }else{
                document.getElementById("logindeusuario").click();
                console.log("no estas logueado, logeate o registrate"); 
            }
        },

        //=========================== CARRITO ====================================
        //ELIMINAR PRODUCTOS DEL PEDIDO
        deleteitem: function(registro, reg, todoitem){
            this.carrito.splice(this.carrito.indexOf(todoitem), 1);                        
            var storedlogin = JSON.parse(localStorage.getItem("loginusuario"));                        

            var delitem = ""
            delitem = delitem + "<PagXml>                                                          ";
            delitem = delitem + "   <CorXml>                                                       ";
            delitem = delitem + "      <TIPO>PARAMETRO</TIPO>                                      ";
            delitem = delitem + "      <PROCESO>0147</PROCESO>                                     "; 
            delitem = delitem + "      <SUCURSAL>00004</SUCURSAL>                                  ";
            delitem = delitem + "      <TIPODOC>PEW</TIPODOC>                                      ";
            delitem = delitem + "      <DOCUMENTO>@@TT1@@</DOCUMENTO>                              ";      
            delitem = delitem + "      <USERID>"+storedlogin.CORREO+"</USERID>                     ";
            delitem = delitem + "      <PASSWORD>"+storedlogin.CLAVE+"</PASSWORD>                  ";         
            delitem = delitem + "      <REG>"+reg+"</REG>                                                "; 
            delitem = delitem + "   </CorXml>                                                      ";
            delitem = delitem + "</PagXml>                                                         ";
            axios({
                method: 'post',
                url: 'https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_carrito.asmx/SetPedido',
                data: "Parametros=" + delitem + "",
                config: { headers: {'Content-Type': 'multipart/form-data' }}
            })
            .then(response => {
                var cadenainicio = response.data,        
                inicio = 90,
                subCadenainicio = cadenainicio.substring(inicio);                              
                var cadena = subCadenainicio,
                patron = "}</string>",
                nuevoValor    = "",
                nuevaCadena = cadena.replace(patron, nuevoValor);            
                var pedidocarrito = JSON.parse(nuevaCadena);   
                
                location.reload();

                this.banderapedido = pedidocarrito.Estado.Registros;                             
                if (this.banderapedido == 0) {
                    location.reload();
                }
                
            })
            .catch(error => {
                console.log(error.response);
            });
        },


        //========================= HACER CHECKOUT Y CAMBIAR ESTADO =======================
        hacercheckout: function(){
            document.getElementById('formcheckout').submit();
            
            var storedlogin = JSON.parse(localStorage.getItem("loginusuario"));

            var webcheckout = ""
                webcheckout = webcheckout + "<PagXml>                                                          ";
                webcheckout = webcheckout + "   <CorXml>                                                       ";
                webcheckout = webcheckout + "      <TIPO>PARAMETRO</TIPO>                                      ";
                webcheckout = webcheckout + "      <PROCESO>0150</PROCESO>                                     "; //registro de usuario
                webcheckout = webcheckout + "      <SUCURSAL>00004</SUCURSAL>                                  ";
                webcheckout = webcheckout + "      <TIPODOC>PEW</TIPODOC>                                      ";
                webcheckout = webcheckout + "      <DOCUMENTO>"+this.referenciacodigo+"</DOCUMENTO>            ";            
                webcheckout = webcheckout + "      <USERID>"+storedlogin.CORREO+"</USERID>                     ";
                webcheckout = webcheckout + "      <PASSWORD>"+storedlogin.CLAVE+"</PASSWORD>                  ";    
                webcheckout = webcheckout + "      <ESTADO>WEBCHECKOUT</ESTADO>                                ";    
                webcheckout = webcheckout + "   </CorXml>                                                      ";
                webcheckout = webcheckout + "</PagXml>                                                         ";     
            axios({
                method: 'post',
                url: 'https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_carrito.asmx/SetPedido',
                data: "Parametros=" + webcheckout + "",
                config: { headers: {'Content-Type': 'multipart/form-data' }}
            })
            .then(response => {
            })
            .catch(error => {
                console.log(error.response);
            });

        },

    },




//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



    created(){

        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }
        
        var merchantId = "00000";
        var referenceCode = "00000";
        var newTXVALUE = "00000";
        var currency = "00000";
        var transactionState = "00000";

        var estadoTx = '';
        var ApiKey = "4Vj8eK4rloUd272L48hsrarnUA";
        var merchantId = getParameterByName('merchantId');
        var referenceCode = getParameterByName('referenceCode');
        var TXVALUE = getParameterByName('TX_VALUE');
        var currency = getParameterByName('currency');
        var transactionState = getParameterByName('transactionState');
        var newTXVALUE = '';

        formatNumber(TXVALUE);

        function formatNumber(num) {
            return newTXVALUE = num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
        }

        //"ApiKey~merchantId~referenceCode~new_value~currency~transactionState"

        //var firmacreada = hex_md5("4Vj8eK4rloUd272L48hsrarnUA~"+merchantId+"~"+referenceCode+"~"+newTXVALUE+"~"+currency+"~"+transactionState);
        var firma = getParameterByName('signature');
        var referencepol = getParameterByName('reference_pol');                    
        var cus = getParameterByName('cus');
        var description = getParameterByName('description');
        var pseBank = getParameterByName('pseBank');
        var lapPaymentMethod = getParameterByName('lapPaymentMethod');
        var transactionId = getParameterByName('transactionId');
        var mensaje = getParameterByName('mensaje');
        
        if (transactionState == 4 ) {
            estadoTx = "Transacción aprobada";
            this.transaccEstado = estadoTx;
        }
        else if (transactionState == 6 ) {
            estadoTx = "Transacción rechazada";
            this.transaccEstado = estadoTx;
        }
        else if (transactionState == 104 ) {
            estadoTx = "Error";
            this.transaccEstado = estadoTx;
        }
        else if (transactionState == 7 ) {
            estadoTx = "Transacción pendiente";
            this.transaccEstado = estadoTx;
        }
        else {
            estadoTx=mensaje;
            this.transaccEstado = estadoTx;
        }  



        let insertscript = document.createElement('script');    
        insertscript.setAttribute('src', "js/main.js");
        document.head.appendChild(insertscript);
        
        // ========================= CARGAR CIUDADES ======================== //
        var self = this;
        var queryciudades = ""
        queryciudades = queryciudades + "<PagXml>                                                 ";
        queryciudades = queryciudades + "   <CorXml>                                              ";
        queryciudades = queryciudades + "      <TIPO>PARAMETRO</TIPO>                             ";
        queryciudades = queryciudades + "      <CODIGO>0139</CODIGO>                              ";
        queryciudades = queryciudades + "      <PAGINA>0</PAGINA>                                 ";
        queryciudades = queryciudades + "      <PAGINAS>0</PAGINAS>                               ";
        queryciudades = queryciudades + "      <NOMBRE>PROCESO</NOMBRE>                           ";
        queryciudades = queryciudades + "      <ORDEN>1</ORDEN>                                   ";
        queryciudades = queryciudades + "      <VALOR><![CDATA[EMPRESA.CONTACTOS]]></VALOR>       ";
        queryciudades = queryciudades + "      <FILTRO><![CDATA[]]></FILTRO>                      ";
        queryciudades = queryciudades + "   </CorXml>                                             ";
        queryciudades = queryciudades + "</PagXml>                                                ";                                                       

        axios({
            method: 'post',
            url: 'https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_carrito.asmx/FiltroBusquedaJson',
            data: "Parametros=" + queryciudades + "",
        })
        .then(function (responseciudad) {
            var cadenacity = responseciudad.data,
            inicio = 90,
            subCadenainicio = cadenacity.substring(inicio);  
            
            var cadena = subCadenainicio,
            patron = "}</string>",
            nuevoValor    = "",
            nuevaCadena = cadena.replace(patron, nuevoValor);            
            var ciudadescarga = JSON.parse(nuevaCadena);
            self.ciudades = ciudadescarga.Datos;
            
        })
        .catch(function (error) {
            console.log(error);
        });
        

        // ========================= CARGAR DEPARTAMENTOS ======================== //
        var querydptos = ""
        querydptos = querydptos + "<PagXml>                                                     ";
        querydptos = querydptos + "   <CorXml>                                                  ";
        querydptos = querydptos + "      <TIPO>PARAMETRO</TIPO>                                 ";
        querydptos = querydptos + "      <CODIGO>0138</CODIGO>                                  ";
        querydptos = querydptos + "      <PAGINA>0</PAGINA>                                     ";
        querydptos = querydptos + "      <PAGINAS>0</PAGINAS>                                   ";
        querydptos = querydptos + "      <NOMBRE>PROCESO</NOMBRE>                               ";
        querydptos = querydptos + "      <ORDEN>1</ORDEN>                                       ";
        querydptos = querydptos + "      <VALOR><![CDATA[EMPRESA.CONTACTOS]]></VALOR>           ";
        querydptos = querydptos + "      <FILTRO><![CDATA[]]></FILTRO>            ";
        querydptos = querydptos + "   </CorXml>                                                 ";
        querydptos = querydptos + "</PagXml>                                                    ";

        axios({
            method: 'post',
            url: 'https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_carrito.asmx/FiltroBusquedaJson',
            data: "Parametros=" + querydptos + "",
        })
        .then(function (responseciudad) {
            var cadenadptos = responseciudad.data,
            inicio = 90,
            subCadenainicio = cadenadptos.substring(inicio);  
            
            var cadena = subCadenainicio,
            patron = "}</string>",
            nuevoValor    = "",
            nuevaCadena = cadena.replace(patron, nuevoValor);            
            var dptoscarga = JSON.parse(nuevaCadena);
            self.departamentos = dptoscarga.Datos;
            
        })
        .catch(function (error) {
            console.log(error);
        });


        //======================= MOSTRAR MARCAS EXISTENTES =======================
        var marcas = ""
        marcas = marcas + "<PagXml>                                               ";
        marcas = marcas + "   <CorXml>                                            ";
        marcas = marcas + "      <TIPO>PARAMETRO</TIPO>                           ";
        marcas = marcas + "      <CODIGO>0155</CODIGO>                            ";
        marcas = marcas + "      <PAGINA>0</PAGINA>                               ";
        marcas = marcas + "      <PAGINAS>0</PAGINAS>                             ";
        marcas = marcas + "      <NOMBRE>PROCESO</NOMBRE>                         ";
        marcas = marcas + "      <ORDEN>1</ORDEN>                                 ";
        marcas = marcas + "      <VALOR><![CDATA[EMPRESA.CONTACTOS]]></VALOR>     ";
        marcas = marcas + "      <FILTRO><![CDATA[]]></FILTRO>                    ";
        marcas = marcas + "   </CorXml>                                           ";
        marcas = marcas + "</PagXml>                                              ";
        
        axios({
            method: 'post',
            url: 'https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_carrito.asmx/FiltroBusquedaJson',
            data: "Parametros=" + marcas + "",
        })
        .then(function (resmarca) {
            var cadenamarcas = resmarca.data,
            inicio = 90,
            subCadenainicio = cadenamarcas.substring(inicio);  
            
            var cadena = subCadenainicio,
            patron = "}</string>",
            nuevoValor    = "",
            nuevaCadena = cadena.replace(patron, nuevoValor);            
            var marcascarga = JSON.parse(nuevaCadena);
            self.marcas = marcascarga.Datos;
            //console.log(self.marcas);
            
        })
        .catch(function (error) {
            console.log(error);
        });


        //======================= MOSTRAR GRUPOS EXISTENTES =======================
        var querygrupos = ""
            querygrupos = querygrupos + "<PagXml>                                              ";
            querygrupos = querygrupos + "   <CorXml>                                           ";
            querygrupos = querygrupos + "      <TIPO>PARAMETRO</TIPO>                          ";
            querygrupos = querygrupos + "      <CODIGO>0156</CODIGO>                           ";
            querygrupos = querygrupos + "      <PAGINA>0</PAGINA>                              ";
            querygrupos = querygrupos + "      <PAGINAS>0</PAGINAS>                            ";
            querygrupos = querygrupos + "      <NOMBRE>PROCESO</NOMBRE>                        ";
            querygrupos = querygrupos + "      <ORDEN>1</ORDEN>                                ";
            querygrupos = querygrupos + "      <VALOR><![CDATA[EMPRESA.CONTACTOS]]></VALOR>    ";
            querygrupos = querygrupos + "      <FILTRO><![CDATA[]]></FILTRO>                   ";
            querygrupos = querygrupos + "   </CorXml>                                          ";
            querygrupos = querygrupos + "</PagXml>                                             ";
            axios({
                method: 'post',
                url: 'https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_carrito.asmx/FiltroBusquedaJson',
                data: "Parametros=" + querygrupos + "",
            })
            .then(function (resgrupos) {
                var cadenagrupos = resgrupos.data,
                inicio = 90,
                subCadenainicio = cadenagrupos.substring(inicio);  
                
                var cadena = subCadenainicio,
                patron = "}</string>",
                nuevoValor    = "",
                nuevaCadena = cadena.replace(patron, nuevoValor);            
                var gruposcarga = JSON.parse(nuevaCadena);
                self.grupos = gruposcarga.Datos;
                //console.log(self.grupos);
                
            })
            .catch(function (error) {
                console.log(error);
            });



        //==============================   TRAER PRODUCTOS DEL CARRITO    ===============================//
        

        //traer los productos de la tabla pedidos, que el cliente ha estado acumulando
        var storedlogin = JSON.parse(localStorage.getItem("loginusuario"));
        
        if (storedlogin == null) {            
            console.log("productos en null");
        } else {
            var consulpedido = ""
                consulpedido = consulpedido + "<PagXml>                                                          ";
                consulpedido = consulpedido + "   <CorXml>                                                       ";
                consulpedido = consulpedido + "      <TIPO>PARAMETRO</TIPO>                                      ";
                consulpedido = consulpedido + "      <PROCESO>0149</PROCESO>                                     "; //registro de usuario
                consulpedido = consulpedido + "      <SUCURSAL>00004</SUCURSAL>                                  ";
                consulpedido = consulpedido + "      <TIPODOC>PEW</TIPODOC>                                      ";
                consulpedido = consulpedido + "      <DOCUMENTO>@@TT1@@</DOCUMENTO>                              ";         
                consulpedido = consulpedido + "      <USERID>"+storedlogin.CORREO+"</USERID>                     ";
                consulpedido = consulpedido + "      <PASSWORD>"+storedlogin.CLAVE+"</PASSWORD>                  ";    
                consulpedido = consulpedido + "   </CorXml>                                                      ";
                consulpedido = consulpedido + "</PagXml>                                                         ";
                axios({
                    method: 'post',
                    url: 'https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_carrito.asmx/SetPedido',
                    data: "Parametros=" + consulpedido + "",
                    config: { headers: {'Content-Type': 'multipart/form-data' }}
                })
                .then(response => {
                    var cadenainicio = response.data,        
                    inicio = 90,
                    subCadenainicio = cadenainicio.substring(inicio);                              
                    var cadena = subCadenainicio,
                    patron = "}</string>",
                    nuevoValor    = "",
                    nuevaCadena = cadena.replace(patron, nuevoValor);            
                    var pedidocarrito = JSON.parse(nuevaCadena);                    
                    
                    //VALIDACION IMPORTANTE llega 1 producto o un grupo de ellos.
                    if (pedidocarrito.Estado.Registros >= 2) {
                        this.carrito = pedidocarrito.Datos;
                        this.referenciacodigo = this.carrito[0].DOCUMENTO;


                        //suma total de los productos dentro del carrito
                        var totales = 0;
                        var valores = 0;
                        var contadorproductos = 0;

                        this.carrito.forEach(element => {                           
                            valores = parseInt(element.VALOR, 10);
                            totales = totales + valores;
                            contadorproductos = contadorproductos + 1;
                        });
                        this.totalcarrito = totales.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                        this.cantidadproductos = contadorproductos;

                        //crear la firma en base al codigo de referencia del pedido 
                        md5string = hex_md5("4Vj8eK4rloUd272L48hsrarnUA~508029~"+this.referenciacodigo+"~"+this.totalcarrito+"~COP");
                        this.md5cadena = md5string;
                        
                    } else if(pedidocarrito.Estado.Registros == 1){
                        var found = Object.values(pedidocarrito);
                        this.carrito = found.slice(0,1);
                        
                        this.referenciacodigo = this.carrito[0].DOCUMENTO;

                        var totales = 0;
                        var valores = 0;
                        var contadorproductos = 1;
                        this.cantidadproductos = contadorproductos;

                        totales = parseInt(this.carrito[0].VALOR, 10);

                        valores = totales.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                        this.totalcarrito = valores;

                        //crear la firma en base al codigo de referencia del pedido
                        md5string = hex_md5("4Vj8eK4rloUd272L48hsrarnUA~508029~"+this.referenciacodigo+"~"+this.totalcarrito+"~COP");
                        this.md5cadena = md5string;  
                    }                     
                })
                .catch(error => {
                    console.log(error.response);
                }); 
        }

        //=========== CARRITO evento submit
        if (document.getElementById("yourLinkId")) {            
            document.getElementById("yourLinkId").onclick = function() {
                document.getElementById("yourFormId").submit();
            }
        }

    },






//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////





    mounted () {
        


        //codigo para enviar al carrito el producto resultado del pop up de productos destacados en HOME index.html
        var popupgocarrito = JSON.parse(localStorage.getItem("popupgocarrito")); 
                
        if (popupgocarrito) {
            console.log(popupgocarrito); 
            
            cantidad = popupgocarrito[0];
            nombre = popupgocarrito[1];
            genero = popupgocarrito[2];
            precio = popupgocarrito[3];
            ref = popupgocarrito[4];
            marca = popupgocarrito[5];
            design = popupgocarrito[6];
            grupo = popupgocarrito[7];
            
            //enviar el producto al pedido del carrito
            this.addcarrito(cantidad, nombre, genero, precio, ref, marca, design, grupo)

        
            //eliminar la session para limpiar el producto seleccionado
            localStorage.removeItem("popupgocarrito");
            localStorage.removeItem("referenciaproductosession");

            // location.reload(); 
        }



        var storedlogin = JSON.parse(localStorage.getItem("loginusuario"));
        //display logout button
        if (storedlogin) {
            $('#logout').removeClass('hiddenclass');
            $('#logout').addClass('displayclass');
            $('.mfp-close').click();                             
        } 
        var self = this;
        var str="";
        var storedlogin = JSON.parse(localStorage.getItem("loginusuario"));
        self.userAuth = storedlogin;
       

        //==================== MOSTRAR TODOS LOS PRODUCTOS EN LA TIENDA ====================      
        if (this.gruposelected) {
            this.Filtro = "SUCURSAL= 00004, GRUPO = "+this.gruposelected+"";
        } else if(this.marcaselected){
            this.Filtro = "SUCURSAL= 00004, MARCA = "+this.marcaselected+"";
        }else {
            this.Filtro = "SUCURSAL= 00004";
        }

        var consulta = ""
            consulta = consulta + "<PagXml>                                                 ";
            consulta = consulta + "   <CorXml>                                              ";
            consulta = consulta + "      <TIPO>PARAMETRO</TIPO>                             ";
            consulta = consulta + "      <CODIGO>0099</CODIGO>                              ";
            consulta = consulta + "      <PAGINA>1</PAGINA>                                 ";
            consulta = consulta + "      <PAGINAS>20000</PAGINAS>                           ";
            consulta = consulta + "      <NOMBRE>PROCESO</NOMBRE>                           ";
            consulta = consulta + "      <ORDEN>1</ORDEN>                                   ";
            consulta = consulta + "      <VALOR><![CDATA[EMPRESA.CONTACTOS]]></VALOR>       ";
            consulta = consulta + "      <FILTRO><![CDATA[" + this.Filtro + "]]></FILTRO>   ";
            consulta = consulta + "   </CorXml>                                             ";
            consulta = consulta + "</PagXml>                                                ";  
            
        axios
        .get('https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_carrito.asmx/FiltroBusquedaJson', {
            params: {
                Parametros: consulta 
            }
        })
        .then( function(res){                           
            var cadenainicio = res.data,
            inicio = 90,
            subCadenainicio = cadenainicio.substring(inicio);  

            var cadena = subCadenainicio,
            patron = "}</string>",
            nuevoValor    = "",
            nuevaCadena = cadena.replace(patron, nuevoValor);            
            var aux = JSON.parse(nuevaCadena);
            self.infos = aux.Datos; 
       
            
        }) 


        //**MUESTRA DETALLES DEL ESTADO DEL PEDIDO DEL CLIENTE
        var storedlogin = JSON.parse(localStorage.getItem("loginusuario")); 
            
            if (storedlogin !== null) {
                var statuspedido = ""
                statuspedido = statuspedido + "<PagXml>                                                          ";
                statuspedido = statuspedido + "   <CorXml>                                                       ";
                statuspedido = statuspedido + "      <TIPO>PARAMETRO</TIPO>                                      ";
                statuspedido = statuspedido + "      <PROCESO>0149</PROCESO>                                     "; //registro de usuario
                statuspedido = statuspedido + "      <SUCURSAL>00004</SUCURSAL>                                  ";
                statuspedido = statuspedido + "      <TIPODOC>PEW</TIPODOC>                                      ";
                statuspedido = statuspedido + "      <DOCUMENTO>@@TT1@@</DOCUMENTO>                              ";         
                statuspedido = statuspedido + "      <USERID>"+storedlogin.CORREO+"</USERID>               ";
                statuspedido = statuspedido + "      <PASSWORD>"+storedlogin.CLAVE+"</PASSWORD>                                 ";    
                statuspedido = statuspedido + "   </CorXml>                                                      ";
                statuspedido = statuspedido + "</PagXml>                                                         "; 
                    
                axios
                .get('https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_carrito.asmx/SetPedido', {
                    params: {
                        Parametros: statuspedido 
                    }
                })
                .then( function(res){                           
                    var cadenainicio = res.data,
                    inicio = 90,
                    subCadenainicio = cadenainicio.substring(inicio);  

                    var cadena = subCadenainicio,
                    patron = "}</string>",
                    nuevoValor    = "",
                    nuevaCadena = cadena.replace(patron, nuevoValor);            
                    var aux = JSON.parse(nuevaCadena);
                    self.statuspedidos = aux.Datos;                    
                })
            }     
    }

})     