
$(document).ready(function () {
   //**AGREGA Y ACTUALIZA UN ITEMS AL PEDIDO
   OrderAddItems = function () {
      var URL = "https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_carrito.asmx/SetPedido";
      //var URL = "http://localhost:47646/GestionMovil/Servicios/sweb_carrito.asmx/SetPedido"

      var consulta = ""
      consulta = consulta + "<PagXml>                                                          ";
      consulta = consulta + "   <CorXml>                                                       ";
      consulta = consulta + "      <TIPO>PARAMETRO</TIPO>                                      ";
      consulta = consulta + "      <PROCESO>0146</PROCESO>                                     "; 
      consulta = consulta + "      <SUCURSAL>00004</SUCURSAL>                                  ";
      consulta = consulta + "      <TIPODOC>PEW</TIPODOC>                                      ";  //FIJO     
      consulta = consulta + "      <DOCUMENTO>@@TT1@@</DOCUMENTO>                              ";  //FIJO     
      consulta = consulta + "      <USERID>stevencuartas@hotmail.com</USERID>               ";
      consulta = consulta + "      <PASSWORD>123123</PASSWORD>                                 ";      
      consulta = consulta + "      <ITEMS>123457</ITEMS>                              ";
      consulta = consulta + "      <COLOR>12345</COLOR>                          ";
      consulta = consulta + "      <TALLA>12345</TALLA>                                   ";
      consulta = consulta + "      <CANTIDAD>20</CANTIDAD>                                       ";
      consulta = consulta + "      <REG>186</REG>                                                "; //si llega registro actualia       
      consulta = consulta + "   </CorXml>                                                      ";
      consulta = consulta + "</PagXml>                                                         ";
      jQuery.ajax({
          type: "POST",
          url: URL,
          data: "Parametros=" + consulta + "",
          success: function (data) {
              var xml_string = jQuery(data).text();
              $("#textAreaFileSelectedAsText").val(xml_string); 
          },
          error: function (xhr, msg) {
              alert(msg + '\n' + xhr.responseText);
          }
      });
   }
   //**QUITA EL REGISTRO DEL PEDIDO
   OrderRemoveItems = function () {
   var URL = "https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_carrito.asmx/SetPedido";
   var URL = "http://localhost:47646/GestionMovil/Servicios/sweb_carrito.asmx/SetPedido"

   var consulta = ""
   consulta = consulta + "<PagXml>                                                          ";
   consulta = consulta + "   <CorXml>                                                       ";
   consulta = consulta + "      <TIPO>PARAMETRO</TIPO>                                      ";
   consulta = consulta + "      <PROCESO>0147</PROCESO>                                     "; 
   consulta = consulta + "      <SUCURSAL>00029</SUCURSAL>                                  ";
   consulta = consulta + "      <TIPODOC>PEW</TIPODOC>                                      ";
   consulta = consulta + "      <DOCUMENTO>@@TT1@@</DOCUMENTO>                              ";      
   consulta = consulta + "      <USERID>jeison.gutierrez@3dev.com.co</USERID>               ";
   consulta = consulta + "      <PASSWORD>123456</PASSWORD>                                 ";         
   consulta = consulta + "      <REG>9</REG>                                                "; 
   consulta = consulta + "   </CorXml>                                                      ";
   consulta = consulta + "</PagXml>                                                         ";
   jQuery.ajax({
       type: "POST",
       url: URL,
       data: "Parametros=" + consulta + "",
       success: function (data) {
           var xml_string = jQuery(data).text();
           $("#textAreaFileSelectedAsText").val(xml_string); 
       },
       error: function (xhr, msg) {
           alert(msg + '\n' + xhr.responseText);
       }
   });
   }
   //**MUESTRA DETALLES DEL METODO DE TRASPORTE
   OrderTransport = function () {
     var URL = "https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_carrito.asmx/SetPedido";
     var URL = "http://localhost:47646/GestionMovil/Servicios/sweb_carrito.asmx/SetPedido"

     var consulta = ""
     consulta = consulta + "<PagXml>                                                          ";
     consulta = consulta + "   <CorXml>                                                       ";
     consulta = consulta + "      <TIPO>PARAMETRO</TIPO>                                      ";
     consulta = consulta + "      <PROCESO>0148</PROCESO>                                     "; //registro de usuario
     consulta = consulta + "      <SUCURSAL>00029</SUCURSAL>                                  ";
     consulta = consulta + "      <TIPODOC>PEW</TIPODOC>                                      ";
     consulta = consulta + "      <DOCUMENTO>@@TT1@@</DOCUMENTO>                              ";      
     consulta = consulta + "      <USERID>jeison.gutierrez@3dev.com.co</USERID>               ";
     consulta = consulta + "      <PASSWORD>123456</PASSWORD>                                 ";    
     consulta = consulta + "   </CorXml>                                                      ";     
     consulta = consulta + "</PagXml>                                                         ";
     jQuery.ajax({
         type: "POST",
         url: URL,
         data: "Parametros=" + consulta + "",
         success: function (data) {
             var xml_string = jQuery(data).text();
             $("#textAreaFileSelectedAsText").val(xml_string); 
         },
         error: function (xhr, msg) {
             alert(msg + '\n' + xhr.responseText);
         }
     });
   }

   
   //**MUESTRA DETALLES DEL ESTADO DEL PEDIDO DEL CLIENTE
   OrderStatus = function (Filtro) {
   var URL = "https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_carrito.asmx/SetPedido";
   var URL = "http://localhost:47646/GestionMovil/Servicios/sweb_carrito.asmx/SetPedido"

   var consulta = ""
   consulta = consulta + "<PagXml>                                                          ";
   consulta = consulta + "   <CorXml>                                                       ";
   consulta = consulta + "      <TIPO>PARAMETRO</TIPO>                                      ";
   consulta = consulta + "      <PROCESO>0149</PROCESO>                                     "; //registro de usuario
   consulta = consulta + "      <SUCURSAL>00029</SUCURSAL>                                  ";
   consulta = consulta + "      <TIPODOC>PEW</TIPODOC>                                      ";
   consulta = consulta + "      <DOCUMENTO>@@TT1@@</DOCUMENTO>                              ";         
   consulta = consulta + "      <USERID>jeison.gutierrez@3dev.com.co</USERID>               ";
   consulta = consulta + "      <PASSWORD>123456</PASSWORD>                                 ";    
   consulta = consulta + "   </CorXml>                                                      ";
   consulta = consulta + "</PagXml>                                                         ";

   jQuery.ajax({
         type: "POST",
         url: URL,
         data: "Parametros=" + consulta + "",
         success: function (data) {
            var xml_string = jQuery(data).text();
            $("#textAreaFileSelectedAsText").val(xml_string); 
         },
         error: function (xhr, msg) {
            alert(msg + '\n' + xhr.responseText);
         }
   });
   }


   
   //**MARCA COMO PAGADO UN PEDIDO 
   OrderUpdateStatus = function (Filtro) {
      var URL = "https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_carrito.asmx/SetPedido";
      var URL = "http://localhost:47646/GestionMovil/Servicios/sweb_carrito.asmx/SetPedido"

      var consulta = ""
      consulta = consulta + "<PagXml>                                                          ";
      consulta = consulta + "   <CorXml>                                                       ";
      consulta = consulta + "      <TIPO>PARAMETRO</TIPO>                                      ";
      consulta = consulta + "      <PROCESO>0150</PROCESO>                                     "; //registro de usuario
      consulta = consulta + "      <SUCURSAL>00004</SUCURSAL>                                  ";
      consulta = consulta + "      <TIPODOC>PEW</TIPODOC>                                      ";
      consulta = consulta + "      <DOCUMENTO>@@TT1@@</DOCUMENTO>                              ";            
      consulta = consulta + "      <USERID>jeison.gutierrez@3dev.com.co</USERID>               ";
      consulta = consulta + "      <PASSWORD>123456</PASSWORD>                                 ";    
      consulta = consulta + "      <ESTADO>WEBCHECKOUT</ESTADO>                                   ";    
      consulta = consulta + "   </CorXml>                                                      ";
      consulta = consulta + "</PagXml>                                                         ";

      jQuery.ajax({
            type: "POST",
            url: URL,
            data: "Parametros=" + consulta + "",
            success: function (data) {
               var xml_string = jQuery(data).text();
               $("#textAreaFileSelectedAsText").val(xml_string); 
            },
            error: function (xhr, msg) {
               alert(msg + '\n' + xhr.responseText);
            }
      });
      }
});