
$(document).ready(function () {

    BuscarMarcas = function (Filtro) {
        var URL = "https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_carrito.asmx/FiltroBusquedaJson";
        var consulta = ""
        consulta = consulta + "<PagXml>                                ";
        consulta = consulta + "   <CorXml>                             ";
        consulta = consulta + "      <TIPO>PARAMETRO</TIPO>            ";
        consulta = consulta + "      <CODIGO>0155</CODIGO>             ";
        consulta = consulta + "      <PAGINA>0</PAGINA>             ";
        consulta = consulta + "      <PAGINAS>0</PAGINAS>           ";
        consulta = consulta + "      <NOMBRE>PROCESO</NOMBRE>          ";
        consulta = consulta + "      <ORDEN>1</ORDEN>                  ";
        consulta = consulta + "      <VALOR><![CDATA[EMPRESA.CONTACTOS]]></VALOR>   ";
        consulta = consulta + "      <FILTRO><![CDATA[" + Filtro + "]]></FILTRO>     ";
        consulta = consulta + "   </CorXml>                             ";
        consulta = consulta + "</PagXml>                               ";
        jQuery.ajax({
            type: "POST",
            url: URL,
            data: "Parametros=" + consulta + "",
            success: function (data) {
                var xml_string = jQuery(data).text();
                $("#textAreaFileSelectedAsText").val(xml_string); 
            },
            error: function (xhr, msg) {
                alert(msg + '\n' + xhr.responseText);
            }
        });
     }
 

     BuscarGrupos = function (Filtro) {
        var URL = "https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_carrito.asmx/FiltroBusquedaJson";
        var consulta = ""
        consulta = consulta + "<PagXml>                                ";
        consulta = consulta + "   <CorXml>                             ";
        consulta = consulta + "      <TIPO>PARAMETRO</TIPO>            ";
        consulta = consulta + "      <CODIGO>0156</CODIGO>             ";
        consulta = consulta + "      <PAGINA>0</PAGINA>             ";
        consulta = consulta + "      <PAGINAS>0</PAGINAS>           ";
        consulta = consulta + "      <NOMBRE>PROCESO</NOMBRE>          ";
        consulta = consulta + "      <ORDEN>1</ORDEN>                  ";
        consulta = consulta + "      <VALOR><![CDATA[EMPRESA.CONTACTOS]]></VALOR>   ";
        consulta = consulta + "      <FILTRO><![CDATA[" + Filtro + "]]></FILTRO>     ";
        consulta = consulta + "   </CorXml>                             ";
        consulta = consulta + "</PagXml>                               ";
        jQuery.ajax({
            type: "POST",
            url: URL,
            data: "Parametros=" + consulta + "",
            success: function (data) {
                var xml_string = jQuery(data).text();
                $("#textAreaFileSelectedAsText").val(xml_string); 
            },
            error: function (xhr, msg) {
                alert(msg + '\n' + xhr.responseText);
            }
        });
     }



    BuscarItemspt = function (Filtro) {
        //var URL = "http://localhost:47646/GestionMovil/Servicios/sweb_carrito.asmx/FiltroBusquedaJson";
        var URL = "https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_carrito.asmx/FiltroBusquedaJson";

        //regresa los productos
        var producto = "";
        if (Filtro == "") {
            Filtro = "SUCURSAL= 00004 ";
        } else {
            Filtro = Filtro + " , SUCURSAL= 00004";
        }

        var consulta = ""
        consulta = consulta + "<PagXml>                                ";
        consulta = consulta + "   <CorXml>                             ";
        consulta = consulta + "      <TIPO>PARAMETRO</TIPO>            ";
        consulta = consulta + "      <CODIGO>0099</CODIGO>             ";
        consulta = consulta + "      <PAGINA>1</PAGINA>             ";
        consulta = consulta + "      <PAGINAS>20</PAGINAS>           ";
        consulta = consulta + "      <NOMBRE>PROCESO</NOMBRE>          ";
        consulta = consulta + "      <ORDEN>1</ORDEN>                  ";
        consulta = consulta + "      <VALOR><![CDATA[EMPRESA.CONTACTOS]]></VALOR>   ";
        consulta = consulta + "      <FILTRO><![CDATA[" + Filtro + "]]></FILTRO>     ";
        consulta = consulta + "   </CorXml>                                          ";
        consulta = consulta + "</PagXml>                                             ";
        jQuery.ajax({
            type: "POST",
            url: URL,
            data: "Parametros=" + consulta + "",
            success: function (data) {
                var xml_string = jQuery(data).text();
                $("#textAreaFileSelectedAsText").val(xml_string);                 
                //console.log(xml_string)
            },
            error: function (xhr, msg) {
                alert(msg + '\n' + xhr.responseText);
            }
        });
    }





    

    BuscarPaginasCarrito = function (Filtro) {
        var URL = "https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_carrito.asmx/PaginasCarrito";
        //regresa la cantidad de paginas del carrito
        
        var consulta = ""
        consulta = consulta + "<PagXml>                                ";
        consulta = consulta + "   <CorXml>                             ";
        consulta = consulta + "      <TIPO>PARAMETRO</TIPO>            ";
        consulta = consulta + "      <CODIGO>0135</CODIGO>             ";
        consulta = consulta + "      <PAGINAS>20</PAGINAS>           ";
        consulta = consulta + "      <SUCURSAL>00004</SUCURSAL>          ";
        consulta = consulta + "   </CorXml>                             ";
        consulta = consulta + "</PagXml>                               ";
        jQuery.ajax({
            type: "POST",
            url: URL,
            data: "Parametros=" + consulta + "",
            success: function (data) {
                var xml_string = jQuery(data).text();
                console.log(xml_string)
            },
            error: function (xhr, msg) {
                alert(msg + '\n' + xhr.responseText);
            }
        });
    }



});