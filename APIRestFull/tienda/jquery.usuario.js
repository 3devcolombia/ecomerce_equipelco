
$(document).ready(function () {


    //**CREA UN USUARIO NUEVO
   UserRegister = function () {
       //var URL = "https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_carrito.asmx/SetData";
       var URL = "http://localhost:47646/GestionMovil/Servicios/sweb_carrito.asmx/SetData"

       var consulta = ""
       consulta = consulta + "<PagXml>                                                          ";
       consulta = consulta + "   <CorXml>                                                       ";
       consulta = consulta + "      <TIPO>PARAMETRO</TIPO>                                      ";
       consulta = consulta + "      <PROCESO>0141</PROCESO>                                     "; //registro de usuario
       consulta = consulta + "      <SUCURSAL>00029</SUCURSAL>                                  ";
       consulta = consulta + "      <NOMBRES>YEISONN ANDRES</NOMBRES>                           ";
       consulta = consulta + "      <APELLIDOS>BENAVIDEZ GUTIERREZ</APELLIDOS>                  ";
       consulta = consulta + "      <IDENTIFICACION>11439650862</IDENTIFICACION>                 ";
       consulta = consulta + "      <CORREO>jeison.gutierrez2@3dev.com.co</CORREO>               ";
       consulta = consulta + "      <TELEFONO>32176434489</TELEFONO>                            ";
       consulta = consulta + "      <DIRECCION>jeison.gutierrez@3dev.com.co</DIRECCION>         ";
       consulta = consulta + "      <DEPARTAMENTO>76</DEPARTAMENTO>                             ";
       consulta = consulta + "      <CIUDAD>76001</CIUDAD>                                       ";
       consulta = consulta + "      <EMPRESA>PROVICOL</EMPRESA>             ";
       consulta = consulta + "      <CODIGO_POSTAL>71001</CODIGO_POSTAL>                        ";
       consulta = consulta + "      <FLAG_FACTURA>1</FLAG_FACTURA>                              ";
       consulta = consulta + "      <CLAVE>12347</CLAVE>                                        ";
       consulta = consulta + "      <REG>0</REG>                                                "; //si llega registro actualia       
       consulta = consulta + "      <VALOR><![CDATA[EMPRESA.CONTACTOS]]></VALOR>                ";
       consulta = consulta + "   </CorXml>                                                      ";
       consulta = consulta + "</PagXml>                                                         ";
       jQuery.ajax({
           type: "POST",
           url: URL,
           data: "Parametros=" + consulta + "",
           success: function (data) {
               var xml_string = jQuery(data).text();
               $("#textAreaFileSelectedAsText").val(xml_string); 
           },
           error: function (xhr, msg) {
               alert(msg + '\n' + xhr.responseText);
           }
       });
   }


   //**CREA UN USUARIO NUEVO
   UserRecuperarContraseña = function () {
    var URL = "https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_carrito.asmx/RecuperaContraseña";
    var URL = "http://localhost:47646/GestionMovil/Servicios/sweb_carrito.asmx/RecuperaContraseña" 

    var consulta = ""
    consulta = consulta + "<PagXml>                                          ";
    consulta = consulta + "   <CorXml>                                       ";
    consulta = consulta + "      <TIPO>PARAMETRO</TIPO>                      ";
    consulta = consulta + "      <PROCESO>0145</PROCESO>                      ";
    consulta = consulta + "      <SUCURSAL>00029</SUCURSAL>                                  ";    
    consulta = consulta + "      <CORREO>alexander_cuartas665@msn.com</CORREO>          ";
    consulta = consulta + "   </CorXml>                             ";
    consulta = consulta + "</PagXml>                               ";
    jQuery.ajax({
        type: "POST",
        url: URL,
        data: "Parametros=" + consulta + "",
        success: function (data) {
            var xml_string = jQuery(data).text();
            $("#textAreaFileSelectedAsText").val(xml_string); 
        },
        error: function (xhr, msg) {
            alert(msg + '\n' + xhr.responseText);
        }
    });
}



   //**AGREGA DIRECCIONES A UN USUARIO EXISTENTE
   UseraddressCreateUpdate = function () {
      var URL = "https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_carrito.asmx/SetData";
      var URL = "http://localhost:47646/GestionMovil/Servicios/sweb_carrito.asmx/SetData" 
      var consulta = ""
      consulta = consulta + "<PagXml>                                                     ";
      consulta = consulta + "   <CorXml>                                                  ";
      consulta = consulta + "      <TIPO>PARAMETRO</TIPO>                                 ";
      consulta = consulta + "      <PROCESO>0143</PROCESO>                                ";
      consulta = consulta + "      <IDENTIFICACION>1143965086</IDENTIFICACION>            ";
      consulta = consulta + "      <CORREO>jeison.gutierrez@3dev.com.co</CORREO>          ";
      consulta = consulta + "      <SUCURSAL>00029</SUCURSAL>                             ";
      consulta = consulta + "      <DEPARTAMENTO>76</DEPARTAMENTO>                        ";
      consulta = consulta + "      <CIUDAD>76xxxx</CIUDAD>                                 ";
      consulta = consulta + "      <FLAG_FACTURA>1</FLAG_FACTURA>                         ";
      consulta = consulta + "      <DIRECCION>jeison.gutierrez@3dev.com.co</DIRECCION>    ";
      consulta = consulta + "      <CODIGO_POSTAL>12344</CODIGO_POSTAL>                   ";
      consulta = consulta + "      <REG>2</REG>                                            "; //si manda registro hace acualizacion de los datos 
      consulta = consulta + "   </CorXml>                                                 ";
      consulta = consulta + "</PagXml>                                                    ";
      jQuery.ajax({
          type: "POST",
          url: URL,
          data: "Parametros=" + consulta + "",
          success: function (data) {
              var xml_string = jQuery(data).text();
              $("#textAreaFileSelectedAsText").val(xml_string); 
          },
          error: function (xhr, msg) {
              alert(msg + '\n' + xhr.responseText);
          }
      });
  }

  //**ELIMINA UNA DIRECCION EXISTENTE
  UseraddressDelete = function (Filtro) {
   var URL = "https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_carrito.asmx/FiltroBusquedaJson";
   var URL = "http://localhost:47646/GestionMovil/Servicios/sweb_carrito.asmx/SetData"    
   var consulta = ""
   consulta = consulta + "<PagXml>                                 ";
   consulta = consulta + "   <CorXml>                              ";
   consulta = consulta + "      <TIPO>PARAMETRO</TIPO>             ";
   consulta = consulta + "      <PROCESO>0144</PROCESO>            ";
   consulta = consulta + "      <IDENTIFICACION>1143965086</IDENTIFICACION>            ";
   consulta = consulta + "      <CORREO>jeison.gutierrez@3dev.com.co</CORREO>          ";
   consulta = consulta + "      <SUCURSAL>00029</SUCURSAL>                             ";   
   consulta = consulta + "      <REG>2</REG>                    ";
   consulta = consulta + "   </CorXml>                             ";
   consulta = consulta + "</PagXml>                                ";
   jQuery.ajax({
       type: "POST",
       url: URL,
       data: "Parametros=" + consulta + "",
       success: function (data) {
           var xml_string = jQuery(data).text();
           $("#textAreaFileSelectedAsText").val(xml_string); 
       },
       error: function (xhr, msg) {
           alert(msg + '\n' + xhr.responseText);
       }
   });
}

  
  
//**PREGUNTA SI UN USUARIO EXISTE
   UserLogin = function () {
      var URL = "https://3dev.com.co/Doom/Bootstrap/Servicios/sweb_carrito.asmx/SetData";
      var URL = "http://localhost:47646/GestionMovil/Servicios/sweb_carrito.asmx/SetData"

      
      var consulta = ""
      consulta = consulta + "<PagXml>                                ";
      consulta = consulta + "   <CorXml>                             ";
      consulta = consulta + "      <TIPO>PARAMETRO</TIPO>            ";
      consulta = consulta + "      <PROCESO>0142</PROCESO>             ";
      consulta = consulta + "      <SUCURSAL>00029</SUCURSAL>                                  ";
      consulta = consulta + "      <CORREO>jeison.gutierrez@3dev.com.co</CORREO>               ";
      consulta = consulta + "      <CLAVE>12347</CLAVE>                                         ";
      consulta = consulta + "   </CorXml>                             ";
      consulta = consulta + "</PagXml>                               ";
      jQuery.ajax({
          type: "POST",
          url: URL,
          data: "Parametros=" + consulta + "",
          success: function (data) {
              var xml_string = jQuery(data).text();
              $("#textAreaFileSelectedAsText").val(xml_string); 

          },
          error: function (xhr, msg) {
              alert(msg + '\n' + xhr.responseText);
          }
      });
  }










});